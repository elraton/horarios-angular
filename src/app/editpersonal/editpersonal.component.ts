import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-editpersonal',
  templateUrl: './editpersonal.component.html',
  styleUrls: ['./editpersonal.component.less']
})
export class EditpersonalComponent implements OnInit {

  options: FormGroup;
  showQuestions = false;
  personal;

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private router: Router,
    private toastr: ToastrService
  ) {
  }

  ngOnInit() {
    this.personal = JSON.parse(localStorage.getItem('edit'));
    this.options = this.fb.group({
      surname: [this.personal.surname, Validators.required],
      name: [this.personal.name, Validators.required],
      dni: [this.personal.dni, Validators.required],
      is_doctor: [this.personal.is_doctor, Validators.required],
      is_kids: [this.personal.is_kids, Validators.required],
      sede: [this.personal.sede, Validators.required],
    });
  }

  save() {

    let formError = false;

    if (!this.options.valid) {
      formError = true;
      this.toastr.error('Debe ingresar todos los campos', '');
    }

    if ( !formError ) {
      const data = {
        id: this.personal.id,
        surname: this.options.get('surname').value,
        name: this.options.get('name').value,
        dni: this.options.get('dni').value,
        is_doctor: this.options.get('is_doctor').value,
        is_kids: this.options.get('is_kids').value,
        sede: this.options.get('sede').value
      };

      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
        })
      };

      this.http.post(localStorage.getItem('baseurl') + '/update_personal', data)
      .subscribe(
        res => {
          if (JSON.parse(JSON.stringify(res)).data === 'ok') {
            this.toastr.success('Se guardo con exito', '');
            setTimeout(() => {
              this.router.navigate(['home']);
            }, 1000);
          } else {
            this.toastr.error('Ocurrio un error al guardar', '');
          }
        },
        error => {
          this.toastr.error('Ocurrio un error al guardar', '');
        }
      );
    }
  }

  cancel() {

  }

}
