import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ToastrService, Toast } from 'ngx-toastr';

@Component({
  selector: 'app-editusers',
  templateUrl: './editusers.component.html',
  styleUrls: ['./editusers.component.less']
})
export class EditusersComponent implements OnInit {

  constructor(
    private http: HttpClient,
    private router: Router,
    private toastr: ToastrService
  ) { }

  username;
  password;
  sede;
  rol;
  id;

  ngOnInit() {
    const user = JSON.parse(localStorage.getItem('user'));
    this.username = user.username;
    this.password = user.password;
    this.sede = user.sede;
    this.rol = user.rol;
    this.id = user.id;
  }

  save() {
    let error = false;
    if (this.username === '' || this.password === '' || this.sede === '' || this.rol === '') {
      error = true;
      this.toastr.error('Debe ingresar todos los datos');
      return;
    }

    if (error === false) {
      const data = {
        id: this.id,
        username: this.username,
        password: this.password,
        sede: this.sede,
        rol: this.rol
      };
      this.http.post(localStorage.getItem('baseurl') + '/update_user', data)
      .subscribe(
        resp => {
          this.toastr.success('se guardo correctamente');
          this.router.navigate(['/home']);
        },
        error2 => {
          console.log(error2);
          this.toastr.error('no se pudo guardar');
        }
      );
    }
  }

}
