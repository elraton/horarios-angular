import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntroComponent } from './intro/intro.component';
import { HomeComponent } from './home/home.component';
import { PersonalComponent } from './personal/personal.component';
import { AddpersonalComponent } from './addpersonal/addpersonal.component';
import { EditpersonalComponent } from './editpersonal/editpersonal.component';

import { SchedulePersonalComponent } from './schedule-personal/schedule-personal.component';
import { AddSchedulePersonalComponent } from './add-schedule-personal/add-schedule-personal.component';
import { EditSchedulePersonalComponent } from './edit-schedule-personal/edit-schedule-personal.component';
import { AttentionComponent } from './attention/attention.component';
import { AddattentionComponent } from './addattention/addattention.component';
import { EditattentionComponent } from './editattention/editattention.component';
import { PatientsComponent } from './patients/patients.component';
import { EditpatientsComponent } from './editpatients/editpatients.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { CalendarComponent } from './calendar/calendar.component';
import { ReprogramCalendarComponent } from './reprogram-calendar/reprogram-calendar.component';
import { PatientControllerComponent } from './patient-controller/patient-controller.component';
import { PatientCalendarComponent } from './patient-calendar/patient-calendar.component';
import { ReportpersonalComponent } from './reportpersonal/reportpersonal.component';
import { AddusersComponent } from './addusers/addusers.component';
import { EditusersComponent } from './editusers/editusers.component';
import { ListusersComponent } from './listusers/listusers.component';
import { PendingboardComponent } from './pendingboard/pendingboard.component';
import { AdddoctorComponent } from './adddoctor/adddoctor.component';

const routes: Routes = [
  { path: '', component: IntroComponent },
  { path: 'home', component: HomeComponent },
  { path: 'personal', component: PersonalComponent },
  { path: 'addpersonal', component: AddpersonalComponent },
  { path: 'editpersonal', component: EditpersonalComponent },

  { path: 'schedulepersonal', component: SchedulePersonalComponent },
  { path: 'addschedulepersonal', component: AddSchedulePersonalComponent },
  { path: 'editschedulepersonal', component: EditSchedulePersonalComponent },

  { path: 'attention', component: AttentionComponent },
  { path: 'addattention', component: AddattentionComponent },
  { path: 'editattention', component: EditattentionComponent },

  { path: 'patients', component: PatientsComponent },
  { path: 'editpatients', component: EditpatientsComponent },

  { path: 'schedule', component: ScheduleComponent},
  { path: 'pendingboard', component: PendingboardComponent},
  { path: 'reprogram/:attention/:calendar', component: ReprogramCalendarComponent},
  { path: 'patient_attentions', component: PatientControllerComponent},
  { path: 'patient_calendar', component: PatientCalendarComponent},

  { path: 'adddoctor', component: AdddoctorComponent},

  { path: 'reportpersonal', component: ReportpersonalComponent},

  { path: 'listuser', component: ListusersComponent},
  { path: 'adduser', component: AddusersComponent},
  { path: 'edituser', component: EditusersComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
