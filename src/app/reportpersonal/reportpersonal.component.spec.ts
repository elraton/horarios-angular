import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportpersonalComponent } from './reportpersonal.component';

describe('ReportpersonalComponent', () => {
  let component: ReportpersonalComponent;
  let fixture: ComponentFixture<ReportpersonalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportpersonalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportpersonalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
