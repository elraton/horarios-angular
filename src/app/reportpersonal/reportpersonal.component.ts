import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Location } from '@angular/common';
import { ToastrService, Toast } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Patient } from '../common/patient';
import { Calendar } from '../common/calendar';
import { Personal } from '../common/personal';

@Component({
  selector: 'app-reportpersonal',
  templateUrl: './reportpersonal.component.html',
  styleUrls: ['./reportpersonal.component.less']
})
export class ReportpersonalComponent implements OnInit {

  options: FormGroup;
  personal = 0;

  personals: Personal[];
  calendarDays: Calendar[];
  patients: Patient[];

  filteredOptions2: Observable<Personal[]>;
  personalControl = new FormControl();

  firstDay: Date;
  lastDay: Date;

  displayedColumns: string[] = ['patient', 'schedule', 'status'];

  showloading = false;
  showtable = false;

  total_attentions = 0;
  attentions_succesfull = 0;
  attentions_failed = 0;
  attentions_reprogramed = 0;

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private router: Router,
    private location: Location,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.options = this.fb.group({
      date_init: ['', Validators.required],
      date_end: ['', Validators.required]
    });

    this.http.get(localStorage.getItem('baseurl') + '/get_personals')
    .subscribe(
      res => {
        this.personals = JSON.parse(JSON.stringify(res)).data;
        // this.personals = this.personals.filter((x) => x.is_doctor === 'n');
        this.filteredOptions2 = this.personalControl.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filter2(value))
        );
      },
      error => {
        console.log(error);
      }
    );

    this.http.get(localStorage.getItem('baseurl') + '/all_patients')
    .subscribe(
      res => {
        this.patients = JSON.parse(JSON.stringify(res)).data;
      },
      error => {
        console.log(error);
      }
    );
  }

  focusDate() {
    console.log('alertita');
  }

  personalChange(id) {
    this.personal = id;
  }

  parsePatient(id: number) {
    const patient = this.patients.find(x => x.id === id);
    return patient.names + ' ' + patient.surnames;
  }

  private _filter2(value: string): Personal[] {
    const filterValue = value.toLowerCase();

    return this.personals.filter((option) => {
      if ( option.surname.toLowerCase().includes(filterValue) ) {
        return option;
      }
      if ( option.name.toLowerCase().includes(filterValue) ) {
        return option;
      }
      if ( option.dni.toLowerCase().includes(filterValue) ) {
        return option;
      }
    });
  }

  save() {
    let error = false;

    if (this.personal <= 0) {
      error = true;
      this.toastr.error('Debe ingresar personal');
    }

    if (!this.options.get('date_init').valid) {
      error = true;
      this.toastr.error('Debe ingresar fecha de inicio');
    }
    if (!this.options.get('date_end').valid) {
      error = true;
      this.toastr.error('Debe ingresar fecha final');
    }

    if (!error) {
      this.calendarDays = [];

      let attentions = [];
      const date_init = new Date(this.options.get('date_init').value).setHours(0, 0, 0);
      const date_end = new Date(this.options.get('date_end').value).setHours(23, 0, 0);

      this.showloading = true;
      this.showtable = false;

      this.total_attentions = 0;
      this.attentions_succesfull = 0;
      this.attentions_failed = 0;
      this.attentions_reprogramed = 0;

      const data = {
        id: this.personal
      };

      this.http.post(localStorage.getItem('baseurl') + '/get_attention_personal', data)
      .subscribe(
        res => {
          attentions = JSON.parse(JSON.stringify(res)).data;

          for (const attention of attentions) {
            const data2 = {
              attention: attention.id
            };
            this.http.post(localStorage.getItem('baseurl') + '/get_calendar_attention', data2)
            .subscribe(
              resp => {
                const calendars = JSON.parse(JSON.stringify(resp)).data;
                for (const cal of calendars) {
                  this.calendarDays.push(cal);
                }
                this.calendarDays = this.calendarDays.filter(
                  x => new Date(x.schedule) > new Date(date_init) && new Date(x.schedule) < new Date(date_end));
                for (const cal of this.calendarDays) {
                  this.total_attentions = this.total_attentions + 1;
                  if (cal.status === 'vino') {
                    this.attentions_succesfull = this.attentions_succesfull + 1;
                  }
                  if (cal.status === 'no vino') {
                    this.attentions_failed = this.attentions_failed + 1;
                  }
                  if (cal.status === 'cambio horario') {
                    this.attentions_reprogramed = this.attentions_reprogramed + 1;
                  }
                }
                this.showtable = true;
                this.showloading = false;
              },
              error2 => {}
            );
          }
        },
        error2 => {}
      );
    }
  }

  cancel() {
    this.location.back();
  }

}
