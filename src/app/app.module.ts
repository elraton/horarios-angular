import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule, MatInputModule, MatSlideToggleModule, MAT_DATE_LOCALE } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ToastrModule } from 'ngx-toastr';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import { MatMenuModule } from '@angular/material/menu';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IntroComponent } from './intro/intro.component';
import { HomeComponent } from './home/home.component';
import { FormComponent } from './form/form.component';
import { CalendarComponent } from './calendar/calendar.component';
import { HeaderComponent } from './header/header.component';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddpersonalComponent } from './addpersonal/addpersonal.component';
import { EditpersonalComponent } from './editpersonal/editpersonal.component';
import { PersonalComponent } from './personal/personal.component';
import { SchedulePersonalComponent } from './schedule-personal/schedule-personal.component';
import { AddSchedulePersonalComponent } from './add-schedule-personal/add-schedule-personal.component';
import { EditSchedulePersonalComponent } from './edit-schedule-personal/edit-schedule-personal.component';
import { AttentionComponent } from './attention/attention.component';
import { AddattentionComponent } from './addattention/addattention.component';
import { EditattentionComponent } from './editattention/editattention.component';
import { PendingboardComponent } from './pendingboard/pendingboard.component';
import { PatientsComponent } from './patients/patients.component';
import { EditpatientsComponent } from './editpatients/editpatients.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { DialogcalendarComponent } from './dialogcalendar/dialogcalendar.component';
import { ReprogramCalendarComponent } from './reprogram-calendar/reprogram-calendar.component';
import { PatientControllerComponent } from './patient-controller/patient-controller.component';
import { PatientCalendarComponent } from './patient-calendar/patient-calendar.component';
import { ReportpersonalComponent } from './reportpersonal/reportpersonal.component';
import { ListusersComponent } from './listusers/listusers.component';
import { AddusersComponent } from './addusers/addusers.component';
import { EditusersComponent } from './editusers/editusers.component';
import { AdddoctorComponent } from './adddoctor/adddoctor.component';

import { registerLocaleData } from '@angular/common';
import es from '@angular/common/locales/es';

registerLocaleData(es);

@NgModule({
  declarations: [
    AppComponent,
    IntroComponent,
    HomeComponent,
    FormComponent,
    CalendarComponent,
    HeaderComponent,
    AddpersonalComponent,
    EditpersonalComponent,
    PersonalComponent,
    SchedulePersonalComponent,
    AddSchedulePersonalComponent,
    EditSchedulePersonalComponent,
    AttentionComponent,
    AddattentionComponent,
    EditattentionComponent,
    PendingboardComponent,
    PatientsComponent,
    EditpatientsComponent,
    ScheduleComponent,
    DialogcalendarComponent,
    ReprogramCalendarComponent,
    PatientControllerComponent,
    PatientCalendarComponent,
    ReportpersonalComponent,
    ListusersComponent,
    AddusersComponent,
    EditusersComponent,
    AdddoctorComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSlideToggleModule,
    AppRoutingModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatTableModule,
    MatPaginatorModule,
    ToastrModule.forRoot(),
    NgxMaterialTimepickerModule.forRoot(),
    MatAutocompleteModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatMenuModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatTooltipModule
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'es-PE' },
    { provide: LOCALE_ID, useValue: 'es-PE' },
  ],
  bootstrap: [AppComponent],
  entryComponents: [DialogcalendarComponent]
})
export class AppModule { }
