import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ToastrService, Toast } from 'ngx-toastr';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.less']
})
export class IntroComponent implements OnInit {
  username = '';
  password = '';

  constructor(
    private http: HttpClient,
    private router: Router,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    const user = localStorage.getItem('user');
    if (user) {
      this.router.navigate(['/home']);
    }
  }

  login() {
    let error = false;
    if (this.username === '' || this.password === '') {
      error = true;
    }

    if (error === false) {
      const data = {
        username: this.username,
        password: this.password
      };

      this.http.post(localStorage.getItem('baseurl') + '/login', data)
      .subscribe(
        res => {
          this.toastr.success('Bienvenido');
          const dat = JSON.parse(JSON.stringify(res)).data;
          localStorage.setItem('user', JSON.stringify(dat));
          this.router.navigate(['/home']);
        },
        error2 => {
          this.toastr.error('Ingreso no permitido');
        }
      );
    }
  }

}
