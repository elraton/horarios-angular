import { Component, OnInit, Inject } from '@angular/core';
import { ToastrService, Toast } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Patient } from '../common/patient';
import { Calendar } from '../common/calendar';
import { Personal } from '../common/personal';
import { Attention } from '../common/attention';
import { Horario } from '../common/horario';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogcalendarComponent } from '../dialogcalendar/dialogcalendar.component';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.less']
})
export class ScheduleComponent implements OnInit {

  today = new Date();
  yesterday = new Date().setDate(this.today.getDate() - 1);
  tomorrow = new Date().setDate(this.today.getDate() + 1);
  sede = '';
  weekday = this.today.getDay();
  horas_adultos = [];
  horas_niños = [];

  // niños mañana
  start_niños_m_hours = 9;
  start_niños_m_minutes = 0;
  end_niños_m_hours = 13;
  end_niños_m_minutes = 0;

  // niños tarde
  start_niños_t_hours = 13;
  start_niños_t_minutes = 30;
  end_niños_t_hours = 19;
  end_niños_t_minutes = 0;

  // adultos mañana
  start_adultos_m_hours = 7;
  start_adultos_m_minutes = 0;
  end_adultos_m_hours = 13;
  end_adultos_m_minutes = 0;

  // adultos tarde
  start_adultos_t_hours = 13;
  start_adultos_t_minutes = 30;
  end_adultos_t_hours = 19;
  end_adultos_t_minutes = 30;

  personals: Personal[] = [];
  patients: Patient[] = [];
  calendarDays: Calendar[] = [];
  horarios: any[] = [];

  calendario = [];

  attentions = [];

  weekdays = ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'];

  is_kids = 'n';
  turno = 'm';
  user;

  constructor(
    private http: HttpClient,
    public dialog: MatDialog,
    private router: Router
  ) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.sede = this.user.sede;
    this.regenerateData();
  }

  regenerateData() {
    const data2 = {
      from: new Date(this.today.setHours(0, 0, 0)),
      to: new Date(this.today.setHours(23, 0, 0))
    };

    this.http.post(localStorage.getItem('baseurl') + '/get_calendar_days', data2)
    .subscribe(
      res => {
        this.calendarDays = JSON.parse(JSON.stringify(res)).data;
        this.http.get(localStorage.getItem('baseurl') + '/get_attentions')
        .subscribe(
          respp => {
            this.attentions = JSON.parse(JSON.stringify(respp)).data;
          },
          errorss => {}
        );
        this.http.get(localStorage.getItem('baseurl') + '/get_all_personal_schedules')
        .subscribe(
          res2 => {
            this.horarios = JSON.parse(JSON.stringify(res2)).data;
            this.http.get(localStorage.getItem('baseurl') + '/get_personals')
            .subscribe(
              res3 => {
                this.personals = JSON.parse(JSON.stringify(res3)).data;
                this.personals = this.personals.filter((x) => x.is_doctor === 'n');
                this.personals = this.personals.filter((x) => x.sede === this.sede);

                // console.log(this.parseDay(this.personals[2]));
                this.createGeneralSchedule();
              },
              error => {
                console.log(error);
              }
            );
          },
          error => {
            console.log(error);
          }
        );
      },
      error => {
        console.log(error);
      }
    );

    this.http.get(localStorage.getItem('baseurl') + '/all_patients')
    .subscribe(
      res => {
        this.patients = JSON.parse(JSON.stringify(res)).data;
      },
      error => {
        console.log(error);
      }
    );

    const curr = this.today;
    if ((curr.getHours() > 13 && curr.getMinutes() > 30) || curr.getHours() > 14) {
      this.turno = 't';
    }
  }

  refreshSchedule() {
    this.createGeneralSchedule();
  }

  createGeneralSchedule() {
    this.calendario = [];
    let personal_aux = JSON.parse(JSON.stringify(this.personals));
    if (this.is_kids === 's') {
      personal_aux = personal_aux.filter(per => per.is_kids === 's');
    } else {
      personal_aux = personal_aux.filter(per => per.is_kids === 'n');
    }
    for (const pp of personal_aux) {
      const arr = this.parseDay(pp);
      let horas = false;
      let count = 0;
      arr.map((item) => {
        if (item.class === 'blank') {
          horas = true;
          count = count + 1;
        }
      });

      if (this.is_kids === 's') {
        if (count < 12) {
          const obj = {
            id: pp.id,
            name: pp.name + ' ' + pp.surname,
            hours: arr
          };
          this.calendario.push(obj);
        }
      } else {
        if (count < 6) {
          const obj = {
            id: pp.id,
            name: pp.name + ' ' + pp.surname,
            hours: arr
          };
          this.calendario.push(obj);
        }
      }
    }
    // console.log(this.parseDay(this.personals[2]));
  }

  changeToday(event) {
    console.log(this.today);
    this.weekday = new Date(this.today).getDay();
    this.regenerateData();
  }

  parseDay(personal: Personal) {
    const newArr = [];
    const st = this.today;
    let newHour = '';
    let endHour = '';
    let hour = 0;
    let min = 0;
    const horario_aux = JSON.parse(JSON.stringify(this.horarios));
    const curr = this.today;
    let attention = 0;
    let calendar = 0;

    if (this.turno === 'm') {
      newHour = '07:00:00';
      endHour = '13:00:00';
      hour = 7;
      min = 0;
    } else {
      newHour = '13:30:00';
      endHour = '19:30:00';
      hour = 13;
      min = 30;
    }

    if (this.weekday === 6) {
      newHour = '07:00:00';
      endHour = '15:00:00';
      hour = 7;
      min = 0;
    }

    while (newHour !== endHour) {
      const aux = st.setHours(hour, min, 0);
      let classP = 'blank';
      let patient = '';
      let p_type = '';
      let interval = '';
      for (const hor of horario_aux.filter(val => val.personal === personal.id && val.day === this.weekdays[curr.getDay()])) {
        const hour_aux = +hor.start_hour.split(':')[0];
        const min_aux = +hor.start_hour.split(':')[1];

        const hour2_aux = +hor.end_hour.split(':')[0];
        const min2_aux = +hor.end_hour.split(':')[1];
        if (hour === hour_aux && min === min_aux) {
          classP = 'open';
        }
        if (hour >= hour_aux && hour <= hour2_aux) {
          classP = 'open';
        }
        if (hour === hour2_aux && min > min2_aux) {
          classP = 'blank';
        }
      }

      for (const yy of this.calendarDays) {
        // if ( new Date(yy.schedule).toString() === new Date(aux).toString()) {
        if ( new Date(yy.schedule).getTime() === new Date( new Date(aux).toLocaleString() ).getTime() ) {

          if (Number(yy.personal) === Number(personal.id)) {
            classP = yy.status;
            attention = yy.attention;
            calendar = yy.id;
            const patt = this.patients.find(val => val.id === yy.patient);
            const att = this.attentions.find(val => val.id === attention);
            interval = att.interval;
            p_type = patt.type;
            if (patt.type !== 'Particular') {
              patient =
                patient + '<b>' + patt.names.toLowerCase() + ' '
                + patt.surnames.toLowerCase() + ' (S)</b><br> ' +
                interval + '<br><span class="separator"></span>';
            } else {
              patient =
                patient + '<b>' + patt.names.toUpperCase() + ' ' +
                patt.surnames.toUpperCase() + ' (P)</b><br> ' +
                interval + '<br><span class="separator"></span>';
            }
          }
        }
      }

      newArr.push(
        {
          time: aux,
          class: classP,
          patient: patient,
          attention: attention,
          calendar: calendar,
          type: p_type,
          interval: interval
        }
      );

      if (personal.is_kids === 's') {
        min = min + 30;
        if (min >= 60) {
          min = 0;
          hour = hour + 1;
        }
      } else {
        hour = hour + 1;
      }
      newHour = (hour < 10 ? '0' + hour : hour) + ':' + (min < 10 ? '0' + min : min) + ':00';
    }
    return newArr;
  }

  setDateTime(schedule) {
    return;
  }

  openMenu(ii) {
    ii.click();
  }

  openDialog(action: string, schedule) {
    const dialogRef = this.dialog.open(DialogcalendarComponent, {
      width: '350px',
      data: {action: action}
    });

    dialogRef.afterClosed().subscribe(result => {
      if ( action === 'Cancelar' && result === 'Cancelar') {
        this.cancelCalendar(schedule);
      }
      if ( action === 'Reprogramar' && result === 'Reprogramar') {
        this.reprogramCalendar(schedule);
      }
    });
  }

  addnewCalendar(calendar) {
    this.http.post(localStorage.getItem('baseurl') + '/store_calendar_day', calendar)
    .subscribe(
      res2 => {
        console.log(res2);
      },
      error => {
        console.log(error);
      }
    );
  }

  editCalendar(calendar) {
    this.http.post(localStorage.getItem('baseurl') + '/update_calendar_day', calendar)
    .subscribe(
      res2 => {
        console.log(res2);
      },
      error => {
        console.log(error);
      }
    );
  }

  reprogramCalendar(schedule) {
    this.router.navigate(['/reprogram/' + schedule.attention + '/' + schedule.calendar]);
  }

  cancelCalendar(schedule) {
    if (+schedule.attention > 0) {
      // get_attention
      const data = {
        id: schedule.attention
      };
      this.http.post(localStorage.getItem('baseurl') + '/get_attention', data)
      .subscribe(
        res => {
          const attention = JSON.parse(JSON.stringify(res)).data;
          const interval: string = attention.interval;
          const hour = attention.hour;
          const hour2 = attention.hour2;
          const data2 = {
            attention: schedule.attention
          };
          this.http.post(localStorage.getItem('baseurl') + '/get_calendar_attention', data2)
          .subscribe(
            res2 => {
              const calendar: Calendar[] = JSON.parse(JSON.stringify(res2)).data;
              const thiscalendar = calendar.find( xx => xx.id === schedule.calendar);
              const lastcalendar = calendar.pop();
              const aux = new Date(lastcalendar.schedule);
              const aux2 = new Date(lastcalendar.schedule);
              let st;
              if (interval === 'Lu-Ma-Mi-Ju-Vi') {
                st = new Date(aux2.setDate(aux.getDate() + 1 ));
                if (st.getDay() === 0) {
                  st = new Date(aux2.setDate(aux.getDate() + 1 ));
                }
                if (st.getDay() === 6) {
                  st = new Date(aux2.setDate(aux.getDate() + 2 ));
                }
              }
              if (interval === 'Lu-Ma-Mi-Ju-Vi-Sa') {
                st = new Date(aux2.setDate(aux.getDate() + 1 ));
                if (st.getDay() === 0) {
                  st = new Date(aux2.setDate(aux.getDate() + 1 ));
                }
                if (st.getDay() === 6) {
                  st.setHours(+hour2.split(':')[0], +hour2.split(':')[1], +hour2.split(':')[2]);
                }
              }

              if (interval === 'Lu-Mi-Vi') {
                st = new Date(aux2.setDate(aux.getDate() + 2 ));
                if (st.getDay() === 0) {
                  st = new Date(aux2.setDate(aux.getDate() + 1 ));
                }
              }
              if (interval === 'Ma-Ju-Sa') {
                st = new Date(aux2.setDate(aux.getDate() + 2 ));
                if (st.getDay() === 1) {
                  st = new Date(aux2.setDate(aux.getDate() + 1 ));
                }
                if (st.getDay() === 6) {
                  st.setHours(+hour2.split(':')[0], +hour2.split(':')[1], +hour2.split(':')[2]);
                }
              }

              const datacalendar = {
                id: thiscalendar.id,
                attention: thiscalendar.attention,
                patient: thiscalendar.patient,
                status: 'no vino',
                comments: thiscalendar.comments,
                schedule: thiscalendar.schedule,
                personal: thiscalendar.personal,
                session: thiscalendar.session
              };
              this.editCalendar(datacalendar);
              const newcalendar = {
                attention: lastcalendar.attention,
                patient: lastcalendar.patient,
                status: 'vino',
                comments: lastcalendar.comments,
                schedule: st.toISOString().split('T')[0] + ' ' + st.toTimeString().split(' ')[0],
                personal: lastcalendar.personal,
                session: lastcalendar.session
              };
              this.addnewCalendar(newcalendar);
              this.regenerateData();
            },
            error => {
              console.log(error);
            }
          );
        },
        error => {
          console.log(error);
        }
      );
    }
  }


  setTomorrow() {
    this.today = new Date(this.tomorrow);
    this.yesterday = new Date().setDate(this.today.getDate() - 1);
    if (new Date(this.yesterday).getDay() === 0) {
      this.yesterday = new Date().setDate(this.today.getDate() - 2);
    }
    this.tomorrow = new Date().setDate(this.today.getDate() + 1);
    if (new Date(this.tomorrow).getDay() === 0) {
      this.tomorrow = new Date().setDate(this.today.getDate() + 2);
    }
    this.weekday = new Date(this.today).getDay();
    this.regenerateData();
  }

  setYesterday() {
    this.today = new Date(this.yesterday);
    this.yesterday = new Date().setDate(this.today.getDate() - 1);
    if (new Date(this.yesterday).getDay() === 0) {
      this.yesterday = new Date().setDate(this.today.getDate() - 2);
    }
    this.tomorrow = new Date().setDate(this.today.getDate() + 1);
    if (new Date(this.tomorrow).getDay() === 0) {
      this.tomorrow = new Date().setDate(this.today.getDate() + 2);
    }
    this.weekday = new Date(this.today).getDay();
    this.regenerateData();
  }

}
