import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSchedulePersonalComponent } from './edit-schedule-personal.component';

describe('EditSchedulePersonalComponent', () => {
  let component: EditSchedulePersonalComponent;
  let fixture: ComponentFixture<EditSchedulePersonalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSchedulePersonalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSchedulePersonalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
