import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Personal } from '../common/personal';
import { Location } from '@angular/common';

@Component({
  selector: 'app-edit-schedule-personal',
  templateUrl: './edit-schedule-personal.component.html',
  styleUrls: ['./edit-schedule-personal.component.less']
})
export class EditSchedulePersonalComponent implements OnInit {

  options: FormGroup;
  showQuestions = false;

  personal: Personal;
  schedule;

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    public router: Router,
    private location: Location,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.personal = JSON.parse(localStorage.getItem('schedule'));
    this.schedule = JSON.parse(localStorage.getItem('edit'));
    this.options = this.fb.group({
      day: [this.schedule.day, Validators.required],
      start_hour: [this.schedule.start_hour, Validators.required],
      end_hour: [this.schedule.end_hour, Validators.required],
      sede: [this.schedule.sede, Validators.required],
      duration: [this.schedule.duration, Validators.required],
    });
  }

  scheduleTable() {
    this.router.navigate(['schedulepersonal']);
  }

  cancel() {
    this.location.back();
  }

  save() {
    let formError = false;
    if (!this.options.valid) {
      formError = true;
      this.toastr.error('Debe ingresar todos los campos');
    }

    if (!formError) {
      const data = {
        id: this.schedule.id,
        personal: this.personal.id,
        day: this.options.get('day').value,
        start_hour: this.options.get('start_hour').value,
        end_hour: this.options.get('end_hour').value,
        sede: this.options.get('sede').value,
        duration: this.options.get('duration').value,
      };
      this.http.post(localStorage.getItem('baseurl') + '/update_personal_schedule', data)
      .subscribe(
        res => {
          if (JSON.parse(JSON.stringify(res)).data === 'ok') {
            this.toastr.success('Se guardo con exito', '');
            this.location.back();
          } else {
            this.toastr.error('Ocurrio un error al guardar', '');
          }
        },
        error => {
          this.toastr.error('Ocurrio un error al guardar', '');
        }
      );
    }
  }

  delete() {
    const data = {
      id: this.schedule.id
    };
    this.http.post(localStorage.getItem('baseurl') + '/destroy_personal_schedule', data)
      .subscribe(
        res => {
          this.toastr.success('Se elimino con exito', '');
          this.location.back();
        },
        error => {
          this.toastr.error('Ocurrio un error al eliminar', '');
        }
      );
  }

}
