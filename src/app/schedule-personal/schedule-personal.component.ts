import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-schedule-personal',
  templateUrl: './schedule-personal.component.html',
  styleUrls: ['./schedule-personal.component.less']
})
export class SchedulePersonalComponent implements OnInit {

  personal;
  data = [];

  constructor(
    public router: Router,
    private http: HttpClient,
  ) { }

  ngOnInit() {
    this.personal = JSON.parse(localStorage.getItem('schedule'));

    const data = {
      id: this.personal.id
    };
    this.http.post(localStorage.getItem('baseurl') + '/get_personal_schedules', data)
    .subscribe(
      res => {
        this.data = JSON.parse(JSON.stringify(res)).data;
        console.log(this.data);
      },
      error => {
        console.log(error);
      }
    );
  }

  parseDay(day) {
    const arr = JSON.parse(JSON.stringify(this.data));
    return arr.filter((vv) => vv.day === day);
  }

  addSchedule() {
    this.router.navigate(['addschedulepersonal']);
  }

  editSchedule(schedule) {
    console.log(schedule);
    localStorage.setItem('edit', JSON.stringify(schedule));
    this.router.navigate(['editschedulepersonal']);
  }

}
