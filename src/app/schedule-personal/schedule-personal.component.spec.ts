import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedulePersonalComponent } from './schedule-personal.component';

describe('SchedulePersonalComponent', () => {
  let component: SchedulePersonalComponent;
  let fixture: ComponentFixture<SchedulePersonalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchedulePersonalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulePersonalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
