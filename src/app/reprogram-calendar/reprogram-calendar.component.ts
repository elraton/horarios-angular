import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastrService, Toast } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Patient } from '../common/patient';
import { Calendar } from '../common/calendar';
import { Attention } from '../common/attention';
import { Personal } from '../common/personal';

@Component({
  selector: 'app-reprogram-calendar',
  templateUrl: './reprogram-calendar.component.html',
  styleUrls: ['./reprogram-calendar.component.less']
})
export class ReprogramCalendarComponent implements OnInit {

  options: FormGroup;
  showQuestions = false;

  patient = 0;
  personal = 0;

  attentionID = 0;
  attention: Attention;
  calendarID = 0;
  calendaritem: Calendar;

  patients: Patient[];
  personals: Personal[];
  calendarDays: Calendar[];

  filteredOptions: Observable<Patient[]>;
  patientControl = new FormControl();

  filteredOptions2: Observable<Personal[]>;
  personalControl = new FormControl();

  doctorCalendar = false;

  firstDay: Date;
  lastDay: Date;
  calendar = [];
  week_next = true;

  medic_attention: Date;

  constructor(
    private http: HttpClient,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe( params => {
      this.attentionID = +params.get('attention');
      this.calendarID = +params.get('calendar');
    });

    const data = {id: this.attentionID };

    this.http.post(localStorage.getItem('baseurl') + '/get_attention', data)
    .subscribe(
      resp => {
        this.attention = JSON.parse(JSON.stringify(resp)).data;
        this.personal = this.attention.personal;
        this.http.get(localStorage.getItem('baseurl') + '/get_personals')
        .subscribe(
          res => {
            this.personals = JSON.parse(JSON.stringify(res)).data;
            this.personals = this.personals.filter((x) => x.is_doctor === 'n');
            this.filteredOptions2 = this.personalControl.valueChanges
            .pipe(
              startWith(''),
              map(value => this._filter2(value))
            );
            const patt = this.personals.find(p => p.id === this.attention.personal);
            this.personalControl.setValue(patt.name + ' ' + patt.surname);

            this.http.get(localStorage.getItem('baseurl') + '/all_patients')
            .subscribe(
              res2 => {
                this.patients = JSON.parse(JSON.stringify(res2)).data;
                this.personalChange(this.personal);

                const data2 = {id: this.calendarID};

                this.http.post(localStorage.getItem('baseurl') + '/get_one_calendar', data2)
                .subscribe(
                  respo => {
                    this.calendaritem = JSON.parse(JSON.stringify(respo)).data;
                    console.log('elcalendario');
                    console.log(this.calendaritem);
                  },
                  error => {
                    console.log(error);
                  }
                );

              },
              error => {
                console.log(error);
              }
            );
          },
          error => {
            console.log(error);
          }
        );
      },
      err => {
        console.log(err);
      }
    );

  }

  focusDate() {
    console.log('alertita');
  }

  patientChange(id) {
    this.patient = id;
  }

  personalChange(id) {
    this.personal = id;

    this.doctorCalendar = true;
    const doctor = this.personals.find(x => Number(x.id) === Number(id));
    const data = {
      id: doctor.id
    };
    const curr = new Date;
    const first = (curr.getDate() - curr.getDay()) + 1;
    const last = first + 5;

    let aux = new Date(curr.setDate(first)).setHours(0, 0, 0);
    this.firstDay = new Date(aux);

    aux = new Date(curr.setDate(last)).setHours(23, 0, 0);
    this.lastDay = new Date(aux);

    const data2 = {
      from: this.firstDay,
      to: this.lastDay
    };

    this.http.post(localStorage.getItem('baseurl') + '/get_calendar_days', data2)
    .subscribe(
      res => {
        this.calendarDays = JSON.parse(JSON.stringify(res)).data;
        console.log(this.calendarDays);
      },
      error => {
        console.log(error);
      }
    );

    this.http.post(localStorage.getItem('baseurl') + '/get_personal_schedules', data)
    .subscribe(
      res => {
        this.calendar = JSON.parse(JSON.stringify(res)).data;
      },
      error => {
        console.log(error);
      }
    );
    //console.log(doctor);
  }

  previus_week() {
    this.week_next = true;
    const curr = new Date;
    const first = (curr.getDate() - curr.getDay()) + 1;
    const last = first + 6;

    let aux = new Date(curr.setDate(first)).setHours(0, 0, 0);
    this.firstDay = new Date(aux);

    aux = new Date(curr.setDate(last)).setHours(0, 0, 0);
    this.lastDay = new Date(aux);

    const data = {
      from: this.firstDay,
      to: this.lastDay
    };

    this.http.post(localStorage.getItem('baseurl') + '/get_calendar_days', data)
    .subscribe(
      res => {
        this.calendarDays = JSON.parse(JSON.stringify(res)).data;
      },
      error => {
        console.log(error);
      }
    );
  }

  next_week() {
    this.week_next = false;
    const curr = new Date;
    const first = (curr.getDate() - curr.getDay()) + 8;
    const last = first + 6;

    let aux = new Date(curr.setDate(first)).setHours(0, 0, 0);
    this.firstDay = new Date(aux);

    aux = new Date(curr.setDate(last)).setHours(0, 0, 0);
    this.lastDay = new Date(aux);

    const data = {
      from: this.firstDay,
      to: this.lastDay
    };

    this.http.post(localStorage.getItem('baseurl') + '/get_calendar_days', data)
    .subscribe(
      res => {
        this.calendarDays = JSON.parse(JSON.stringify(res)).data;
      },
      error => {
        console.log(error);
      }
    );

    const curr2 = new Date;
    const st = new Date(curr2.setDate(this.firstDay.getDate() + 4));
    const aux2 = st.setHours(13, 30, 0);
  }

  parseDay(day, weekday) {
    let arr = JSON.parse(JSON.stringify(this.calendar));
    arr = arr.filter((vv) => vv.day === day);
    let newArr = [];
    const curr = new Date;
    curr.setHours(9, 0, 0);
    let st = new Date(curr.setDate(this.firstDay.getDate() + weekday));
    for ( const xx of arr ) {
      let newHour = xx.start_hour;
      let hour = Number(newHour.split(':')[0]);
      const min_hour = Number(newHour.split(':')[1]);
      let min = Number(newHour.split(':')[1]);
      let aux = st.setHours(newHour.split(':')[0], newHour.split(':')[1], 0);
      let classP = 'open';
      let patient = '';
      for (const yy of this.calendarDays) {
        if ( new Date(yy.schedule).toString() === new Date(aux).toString()) {
          if (Number(yy.personal) === Number(this.personal)) {
            classP = 'closed';
            const patt = this.patients.find(val => val.id === yy.patient);
            patient = patt.names + ' ' + patt.surnames;
          }
        }
      }
      newArr.push(
        {
          time: aux,
          class: classP,
          patient: patient
        }
      );
      while (newHour !== xx.end_hour) {
        if (xx.is_kids === 's') {
          min = min + 30;
          if (min === 60) {
            hour = hour + 1;
            min = 0;
          }
        } else {
          hour = hour + 1;
        }

        if (hour < 10) {
          newHour = '0' + hour;
        } else {
          newHour = hour;
        }
        if (min < 10) {
          newHour = newHour + ':0' + min;
        } else {
          newHour = newHour + ':' + min;
        }
        newHour = newHour + ':00';
        aux = st.setHours(newHour.split(':')[0], newHour.split(':')[1], 0);
        classP = 'open';
        patient = '';
        for (const yy of this.calendarDays) {
          if ( new Date(yy.schedule).toString() === new Date(aux).toString()) {
            if (Number(yy.personal) === Number(this.personal)) {
              classP = 'closed';
              const patt = this.patients.find(val => val.id === yy.patient);
              patient = patt.names + ' ' + patt.surnames;
            }
          }
        }
        newArr.push(
          {
            time: aux,
            class: classP,
            patient: patient
          }
        );
      }
      // st.setHours(xx.start_hour.split(':')[0], xx.start_hour.split(':')[1]);
      newArr.pop();
    }
    return newArr;
  }

  setDateTime(time) {
    console.log(new Date(time.time).toString());
    if (time.class === 'open') {
      this.medic_attention = time.time;
    }
  }

  getClass(time) {
    if (time.class === 'open') {
      if (time.time !== undefined && this.medic_attention !== undefined) {
        if ( new Date(time.time).toString() === new Date(this.medic_attention).toString() ) {
          return 'selected';
        } else {
          return '';
        }
      }
    }
  }

  save() {
    let error = false;
    console.log(new Date(this.medic_attention).toString());
    console.log(this.personal);
    if (!this.medic_attention) {
      this.toastr.error('Seleccione un nuevo horario para reprogramar');
      error = true;
    }
    if (this.personal <= 0) {
      this.toastr.error('Debe elegir un fisioterapeuta');
      error = true;
    }
    if (error === false) {
      const data = {
        id: this.calendarID,
        attention: this.calendaritem.attention,
        patient: this.calendaritem.patient,
        status: 'cambio horario',
        comments: this.calendaritem.comments,
        schedule: this.calendaritem.schedule,
        personal: this.calendaritem.personal,
        session: this.calendaritem.session
      };

      this.http.post(localStorage.getItem('baseurl') + '/update_calendar_day', data)
      .subscribe(
        res => {
          console.log(res);
          if (JSON.parse(JSON.stringify(res)).data === 'ok') {
            const data2 = {
              attention: this.calendaritem.attention,
              patient: this.calendaritem.patient,
              status: 'vino',
              comments: this.calendaritem.comments,
              schedule: new Date(this.medic_attention).toISOString().split('T')[0] + ' ' + new Date(this.medic_attention).toTimeString().split(' ')[0],
              personal: this.personal,
              session: this.calendaritem.session
            };
            this.http.post(localStorage.getItem('baseurl') + '/store_calendar_day', data2)
            .subscribe(
              resp => {
                if (JSON.parse(JSON.stringify(res)).data === 'ok') {
                  this.toastr.success('Se guardo con exito', '');
                  this.router.navigate(['/schedule']);
                } else {
                  this.toastr.error('Ocurrio un error al guardar', '');
                }
              },
              error2 => {
                console.log(error2);
              }
            );
          } else {
            this.toastr.error('Ocurrio un error al guardar', '');
          }
        },
        error1 => {
          this.toastr.error('Ocurrio un error al guardar', '');
        }
      );
    }
  }

  cancel() {
    this.router.navigate(['/schedule']);
  }

  private _filter2(value: string): Personal[] {
    const filterValue = value.toLowerCase();

    return this.personals.filter((option) => {
      if ( option.surname.toLowerCase().includes(filterValue) ) {
        return option;
      }
      if ( option.name.toLowerCase().includes(filterValue) ) {
        return option;
      }
      if ( option.dni.toLowerCase().includes(filterValue) ) {
        return option;
      }
    });
  }

  private _filter(value: string): Patient[] {
    const filterValue = value.toLowerCase();

    return this.patients.filter((option) => {
      if ( option.surnames.toLowerCase().includes(filterValue) ) {
        return option;
      }
      if ( option.names.toLowerCase().includes(filterValue) ) {
        return option;
      }
      if ( option.dni.toLowerCase().includes(filterValue) ) {
        return option;
      }
    });
  }
}

