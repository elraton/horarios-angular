import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReprogramCalendarComponent } from './reprogram-calendar.component';

describe('ReprogramCalendarComponent', () => {
  let component: ReprogramCalendarComponent;
  let fixture: ComponentFixture<ReprogramCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReprogramCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReprogramCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
