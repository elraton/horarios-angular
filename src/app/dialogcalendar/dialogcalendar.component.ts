import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialogcalendar',
  templateUrl: './dialogcalendar.component.html',
  styleUrls: ['./dialogcalendar.component.less']
})
export class DialogcalendarComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DialogcalendarComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }

}

export interface DialogData {
  action: string;
}
