import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditattentionComponent } from './editattention.component';

describe('EditattentionComponent', () => {
  let component: EditattentionComponent;
  let fixture: ComponentFixture<EditattentionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditattentionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditattentionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
