import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Calendar } from '../common/calendar';
import { DialogcalendarComponent } from '../dialogcalendar/dialogcalendar.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-patient-calendar',
  templateUrl: './patient-calendar.component.html',
  styleUrls: ['./patient-calendar.component.less']
})
export class PatientCalendarComponent implements OnInit {

  constructor(
    private http: HttpClient,
    private router: Router,
    public dialog: MatDialog,
  ) { }

  attentionID = 0;
  calendar: Calendar[] = [];
  days = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];

  ngOnInit() {
    this.regenerateData();
  }

  getdayofweek(date: Date) {
    return this.days[new Date(date).getDay()];
  }

  openDialog(action: string, schedule) {
    const dialogRef = this.dialog.open(DialogcalendarComponent, {
      width: '350px',
      data: {action: action}
    });

    dialogRef.afterClosed().subscribe(result => {
      if ( action === 'Cancelar' && result === 'Cancelar') {
        this.cancelCalendar(schedule);
      }
      if ( action === 'Reprogramar' && result === 'Reprogramar') {
        this.reprogramCalendar(schedule);
      }
    });
  }

  regenerateData() {
    this.attentionID = +localStorage.getItem('attention');
    const data = {
      attention: this.attentionID,
    };
    this.http.post(localStorage.getItem('baseurl') + '/get_calendar_attention', data).subscribe(
      res => {
        this.calendar = JSON.parse(JSON.stringify(res)).data;
      },
      error => {
        console.log(error);
      }
    );
  }

  reprogramCalendar(schedule) {
    this.router.navigate(['/reprogram/' + schedule.attention + '/' + schedule.id]);
  }

  cancelCalendar(schedule) {
    console.log(schedule);
    if (+schedule.attention > 0) {
      // get_attention
      const data = {
        id: schedule.attention
      };
      this.http.post(localStorage.getItem('baseurl') + '/get_attention', data)
      .subscribe(
        res => {
          const attention = JSON.parse(JSON.stringify(res)).data;
          const interval: string = attention.interval;
          const hour = attention.hour;
          const hour2 = attention.hour2;
          const data2 = {
            attention: schedule.attention
          };
          this.http.post(localStorage.getItem('baseurl') + '/get_calendar_attention', data2)
          .subscribe(
            res2 => {
              const calendar: Calendar[] = JSON.parse(JSON.stringify(res2)).data;
              const thiscalendar = calendar.find( xx => xx.id === schedule.id);
              const lastcalendar = calendar.pop();
              const aux = new Date(lastcalendar.schedule);
              const aux2 = new Date(lastcalendar.schedule);
              let st;
              if (interval === 'Lu-Ma-Mi-Ju-Vi') {
                st = new Date(aux2.setDate(aux.getDate() + 1 ));
                if (st.getDay() === 0) {
                  st = new Date(aux2.setDate(aux.getDate() + 1 ));
                }
                if (st.getDay() === 6) {
                  st = new Date(aux2.setDate(aux.getDate() + 2 ));
                }
              }
              if (interval === 'Lu-Ma-Mi-Ju-Vi-Sa') {
                st = new Date(aux2.setDate(aux.getDate() + 1 ));
                if (st.getDay() === 0) {
                  st = new Date(aux2.setDate(aux.getDate() + 1 ));
                }
                if (st.getDay() === 6) {
                  st.setHours(+hour2.split(':')[0], +hour2.split(':')[1], +hour2.split(':')[2]);
                }
              }

              if (interval === 'Lu-Mi-Vi') {
                st = new Date(aux2.setDate(aux.getDate() + 2 ));
                if (st.getDay() === 0) {
                  st = new Date(aux2.setDate(aux.getDate() + 1 ));
                }
              }
              if (interval === 'Ma-Ju-Sa') {
                st = new Date(aux2.setDate(aux.getDate() + 2 ));
                if (st.getDay() === 1) {
                  st = new Date(aux2.setDate(aux.getDate() + 1 ));
                }
                if (st.getDay() === 6) {
                  st.setHours(+hour2.split(':')[0], +hour2.split(':')[1], +hour2.split(':')[2]);
                }
              }

              const datacalendar = {
                id: thiscalendar.id,
                attention: thiscalendar.attention,
                patient: thiscalendar.patient,
                status: 'no vino',
                comments: thiscalendar.comments,
                schedule: thiscalendar.schedule,
                personal: thiscalendar.personal,
                session: thiscalendar.session
              };
              this.editCalendar(datacalendar);
              const newcalendar = {
                attention: lastcalendar.attention,
                patient: lastcalendar.patient,
                status: 'vino',
                comments: lastcalendar.comments,
                schedule: st.toISOString().split('T')[0] + ' ' + st.toTimeString().split(' ')[0],
                personal: lastcalendar.personal,
                session: lastcalendar.session
              };
              this.addnewCalendar(newcalendar);
            },
            error => {
              console.log(error);
            }
          );
        },
        error => {
          console.log(error);
        }
      );
    }
  }

  addnewCalendar(calendar) {
    this.http.post(localStorage.getItem('baseurl') + '/store_calendar_day', calendar)
    .subscribe(
      res2 => {
        console.log(res2);
        this.regenerateData();
      },
      error => {
        console.log(error);
      }
    );
  }

  editCalendar(calendar) {
    this.http.post(localStorage.getItem('baseurl') + '/update_calendar_day', calendar)
    .subscribe(
      res2 => {
        console.log(res2);
        this.regenerateData();
      },
      error => {
        console.log(error);
      }
    );
  }

}
