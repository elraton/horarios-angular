import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})

export class AppComponent {
  title = 'calendarApp';
  showheader = false;
  constructor(
    location: Location,
    router: Router,
  ) {
    localStorage.setItem('baseurl', 'http://horarios.laisladelraton.com/api');
    if ( location.path().length > 0 ) {
      this.showheader = true;
    } else {
      this.showheader = false;
    }
    const user = localStorage.getItem('user');
    if (user) {

    } else {
      router.navigate(['/']);
    }
  }
}
