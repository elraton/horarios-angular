import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {

  constructor(
    private router: Router,
    private http: HttpClient,
  ) { }

  user;
  pending_count = 0;

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.http.get(localStorage.getItem('baseurl') + '/get_pending_boards')
    .subscribe(
      res => {
        let pendings = JSON.parse(JSON.stringify(res)).data;
        pendings = pendings.filter( x => x.user === this.user.id);
        this.pending_count = pendings.length;
      },
      error => {
        console.log(error);
      }
    );
  }

  logout() {
    localStorage.removeItem('user');
    this.router.navigate(['/']);
  }

}
