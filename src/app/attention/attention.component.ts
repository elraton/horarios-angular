import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { Attention } from '../common/attention';
import { DoctorAttention } from '../common/doctor_attention';
import { Personal } from '../common/personal';
import { Patient } from '../common/patient';
import { GlobalAttention } from '../common/global_attention';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { relativeTimeRounding } from 'moment';

@Component({
  selector: 'app-attention',
  templateUrl: './attention.component.html',
  styleUrls: ['./attention.component.less']
})
export class AttentionComponent implements OnInit {

  displayedColumns: string[] = [
    'patient',
    'sessions',
    'startdate',
    'interval',
    'hour',
    'personal',
    'medic_attention',
    'status',
    'delete',
    'reg_medic'
  ];
  dataSource;
  personal: Personal[] = [];
  patients: Patient[] = [];
  globalattentions: GlobalAttention[] = [];
  attentions: Attention[] = [];
  doctorattentions: DoctorAttention[] = [];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  ngOnInit() {
    this.http.get(localStorage.getItem('baseurl') + '/get_global_attentions')
    .subscribe(
      response => {
        this.globalattentions = JSON.parse(JSON.stringify(response)).data;

        this.http.get(localStorage.getItem('baseurl') + '/get_attentions')
        .subscribe(
          res => {
            this.attentions = JSON.parse(JSON.stringify(res)).data;
            this.dataSource = new MatTableDataSource<GlobalAttention>(this.globalattentions);
            this.dataSource.paginator = this.paginator;
          },
          error => {
            console.log(error);
          }
        );
      },
      error => {
        console.log(error);
      }
    );

    this.http.get(localStorage.getItem('baseurl') + '/get_doctor_attentions')
    .subscribe(
      res => {
        this.doctorattentions = JSON.parse(JSON.stringify(res)).data;
      },
      error => {
        console.log(error);
      }
    );

    this.http.get(localStorage.getItem('baseurl') + '/all_patients')
    .subscribe(
      res => {
        this.patients = JSON.parse(JSON.stringify(res)).data;
      },
      error => {
        console.log(error);
      }
    );

    this.http.get(localStorage.getItem('baseurl') + '/get_personals')
    .subscribe(
      res => {
        this.personal = JSON.parse(JSON.stringify(res)).data;
        this.personal = this.personal.filter((x) => x.is_doctor === 'n');
      },
      error => {
        console.log(error);
      }
    );
  }

  editAttention(attention) {
    console.log(this.attentions);
    const att = this.attentions.find(x => x.global === attention.id);
    console.log(att);
    if (att !== undefined) {
      localStorage.setItem('edit', JSON.stringify(att));
      this.router.navigate(['editattention']);
    }
  }

  deletePersonal(personal) {
    console.log('se va a borrar');
    // route: destroy_personal
  }

  parsePatient(id: number) {
    if (this.patients.length > 0) {
      const patient = this.patients.find(x => x.id === id);
      return patient.names + ' ' + patient.surnames;
    } else {
      return '';
    }
  }

  schedulePersonal(personal) {
    localStorage.setItem('schedule', JSON.stringify(personal));
    this.router.navigate(['schedulepersonal']);
  }

  parsePersonal(id) {
    const att = this.attentions.find(x => x.global === id);
    if (att !== undefined) {
      if (this.personal.length > 0) {
        const pp = this.personal.find(val => val.id === att.personal);
        return pp.name + ' ' + pp.surname;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }

  getSessions(id) {
    const att = this.attentions.find(x => x.global === id);
    if (att !== undefined) {
      return att.sessions;
    } else {
      return '';
    }
  }

  getStartDate(id) {
    const att = this.attentions.find(x => x.global === id);
    if (att !== undefined) {
      return att.startdate;
    } else {
      return '';
    }
  }

  getInterval(id) {
    const att = this.attentions.find(x => x.global === id);
    if (att !== undefined) {
      return att.interval;
    } else {
      return '';
    }
  }

  getHour(id) {
    const att = this.attentions.find(x => x.global === id);
    if (att !== undefined) {
      return att.hour;
    } else {
      return '';
    }
  }

  getMedicConsult(id) {
    const att = this.doctorattentions.find(x => x.global === id);
    if (att === undefined) {
      return 'no';
    } else {
      return 'si';
    }
  }

  doctorRegularice(element) {
    localStorage.setItem('attention', JSON.stringify(element));
    this.router.navigate(['/adddoctor']);
  }

}
