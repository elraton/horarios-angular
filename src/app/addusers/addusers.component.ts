import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ToastrService, Toast } from 'ngx-toastr';

@Component({
  selector: 'app-addusers',
  templateUrl: './addusers.component.html',
  styleUrls: ['./addusers.component.less']
})
export class AddusersComponent implements OnInit {

  constructor(
    private http: HttpClient,
    private router: Router,
    private toastr: ToastrService
  ) { }

  username = '';
  password = '';
  sede = '';
  rol = '';

  ngOnInit() {
  }

  save() {
    let error = false;
    if (this.username === '' || this.password === '' || this.sede === '' || this.rol === '') {
      error = true;
      this.toastr.error('Debe ingresar todos los datos');
      return;
    }

    if (error === false) {
      const data = {
        username: this.username,
        password: this.password,
        sede: this.sede,
        rol: this.rol
      };
      this.http.post(localStorage.getItem('baseurl') + '/save_user', data)
      .subscribe(
        resp => {
          this.toastr.success('se guardo correctamente');
          this.router.navigate(['/home']);
        },
        error2 => {
          console.log(error2);
          this.toastr.error('no se pudo guardar');
        }
      );
    }
  }

}
