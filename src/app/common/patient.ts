export interface Patient {
    id: Number;
    surnames: string;
    names: string;
    dni: string;
    type: string;
    traumatologist: boolean;
    physiatrist: boolean;
    operation: boolean;
    fracture: boolean;
    accident: boolean;
    medic_attention: string;
    doctor: string;
}
