export interface Calendar {
    id: number;
    attention: number;
    patient: number;
    status: string;
    comments: string;
    schedule: Date;
    personal: number;
    session: number;
}
