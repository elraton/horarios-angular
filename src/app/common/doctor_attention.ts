export interface DoctorAttention {
    id: number;
    patient: number;
    personal: number;
    sede: string;
    schedule: Date;
    global: number;
}
