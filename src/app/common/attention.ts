export interface Attention {
    id: number;
    patient: Number;
    sessions: Number;
    startdate: Date;
    interval: Number;
    hour: string;
    hour2: string;
    personal: number;
    global: number;
}
