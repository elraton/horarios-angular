export interface Pending {
    id: number;
    patient: number;
    attention: number;
    message: string;
    level: number;
    user: number;
}
