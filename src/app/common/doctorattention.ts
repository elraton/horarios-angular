export interface DoctorAttention {
    id: number;
    patient: Number;
    personal: Number;
    sede: string;
    schedule: Date;
    global: number;
}
