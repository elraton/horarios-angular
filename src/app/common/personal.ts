export interface Personal {
  id: number;
  name: string;
  surname: string;
  dni: string;
  is_doctor: string;
  is_kids: string;
  sede: string;
}
