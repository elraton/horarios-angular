import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSchedulePersonalComponent } from './add-schedule-personal.component';

describe('AddSchedulePersonalComponent', () => {
  let component: AddSchedulePersonalComponent;
  let fixture: ComponentFixture<AddSchedulePersonalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSchedulePersonalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSchedulePersonalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
