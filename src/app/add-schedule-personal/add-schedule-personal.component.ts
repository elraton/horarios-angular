import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Personal } from '../common/personal';
import { Location } from '@angular/common';

@Component({
  selector: 'app-add-schedule-personal',
  templateUrl: './add-schedule-personal.component.html',
  styleUrls: ['./add-schedule-personal.component.less']
})
export class AddSchedulePersonalComponent implements OnInit {
  options: FormGroup;
  showQuestions = false;

  personal: Personal;

  constructor(
    fb: FormBuilder,
    private http: HttpClient,
    public router: Router,
    private location: Location,
    private toastr: ToastrService
  ) {
    this.options = fb.group({
      day: ['', Validators.required],
      start_hour: ['', Validators.required],
      end_hour: ['', Validators.required],
      sede: ['', Validators.required],
      duration: ['', Validators.required],
    });
  }

  ngOnInit() {
    this.personal = JSON.parse(localStorage.getItem('schedule'));
    this.options.get('sede').setValue(this.personal.sede);
  }

  scheduleTable() {
    this.router.navigate(['schedulepersonal']);
  }

  cancel() {
    this.location.back();
  }

  save() {
    let formError = false;
    if (!this.options.valid) {
      formError = true;
      this.toastr.error('Debe ingresar todos los campos');
    }

    if (!formError) {
      const data = {
        personal: this.personal.id,
        day: this.options.get('day').value,
        start_hour: this.options.get('start_hour').value,
        end_hour: this.options.get('end_hour').value,
        sede: this.options.get('sede').value,
        duration: this.options.get('duration').value,
      };
      this.http.post(localStorage.getItem('baseurl') + '/store_personal_schedule', data)
      .subscribe(
        res => {
          if (JSON.parse(JSON.stringify(res)).data === 'ok') {
            this.toastr.success('Se guardo con exito', '');
          } else {
            this.toastr.error('Ocurrio un error al guardar', '');
          }
        },
        error => {
          this.toastr.error('Ocurrio un error al guardar', '');
        }
      );
    }
  }

}
