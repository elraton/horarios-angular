import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {

  options: FormGroup;
  showQuestions = false;
  personal = [];
  patients = [];
  doctorCalendar = false;
  calendar = [];

  week_next = true;

  firstDay: Date;
  lastDay: Date;

  medic_attention: Date;

  constructor(
    private router: Router,
    fb: FormBuilder,
    private http: HttpClient,
    private toastr: ToastrService
  ) {
    this.options = fb.group({
      surname: ['', Validators.required],
      name: ['', Validators.required],
      dni: ['', Validators.required],
      type: ['Seguro EPS', Validators.required],
      traum: ['n', Validators.required],
      fisiat: ['n', Validators.required],
      oper: ['n'],
      fractu: ['n'],
      accid: ['n']
    });
  }

  ngOnInit() {
    this.http.get(localStorage.getItem('baseurl') + '/get_personals')
    .subscribe(
      res => {
        this.personal = JSON.parse(JSON.stringify(res)).data;
        this.personal = this.personal.filter((x) => x.is_doctor === 's');
      },
      error => {
        console.log(error);
      }
    );
  }

  gotoForm() {
    this.router.navigate(['form']);
  }
  gotoCalendar() {
    this.router.navigate(['calendar']);
  }

  previus_week() {
    this.week_next = true;
    const curr = new Date;
    const first = (curr.getDate() - curr.getDay()) + 1;
    const last = first + 5;

    let aux = new Date(curr.setDate(first)).setHours(0, 0, 0);
    this.firstDay = new Date(aux);

    aux = new Date(curr.setDate(last)).setHours(0, 0, 0);
    this.lastDay = new Date(aux);

    const data = {
      from: this.firstDay,
      to: this.lastDay
    };

    this.http.post(localStorage.getItem('baseurl') + '/get_patients', data)
    .subscribe(
      res => {
        this.patients = JSON.parse(JSON.stringify(res)).data;
        console.log('patients');
        console.log(this.patients);
      },
      error => {
        console.log(error);
      }
    );
  }

  next_week() {
    this.week_next = false;
    const curr = new Date;
    const first = (curr.getDate() - curr.getDay()) + 8;
    const last = first + 5;

    let aux = new Date(curr.setDate(first)).setHours(0, 0, 0);
    this.firstDay = new Date(aux);

    aux = new Date(curr.setDate(last)).setHours(0, 0, 0);
    this.lastDay = new Date(aux);

    const data = {
      from: this.firstDay,
      to: this.lastDay
    };

    this.http.post(localStorage.getItem('baseurl') + '/get_patients', data)
    .subscribe(
      res => {
        this.patients = JSON.parse(JSON.stringify(res)).data;
        console.log('patients');
        console.log(this.patients);
      },
      error => {
        console.log(error);
      }
    );
  }

  aParticular(event) {
    if (event.value === 'Particular') {
      this.showQuestions = true;
    } else {
      this.showQuestions = false;
    }
  }

  parseDay(day, weekday) {
    let arr = JSON.parse(JSON.stringify(this.calendar));
    arr = arr.filter((vv) => vv.day === day);
    let newArr = [];
    const curr = new Date;
    for ( const xx of arr ) {
      let st = new Date(curr.setDate(this.firstDay.getDate() + weekday));
      let newHour = xx.start_hour;
      let hour = Number(newHour.split(':')[0]);
      let min = Number(newHour.split(':')[1]);
      let aux = st.setHours(newHour.split(':')[0], newHour.split(':')[1], 0);
      let classP = 'open';
      let patient = '';
      for (const yy of this.patients) {
        if ( new Date(yy.medic_attention).toString() === new Date(aux).toString()) {
          if (yy.doctor === this.options.get('doctor').value) {
            classP = 'closed';
            patient = yy.names + ' ' + yy.surnames;
          }
        }
      }
      newArr.push(
        {
          time: aux,
          class: classP,
          patient: patient
        }
      );
      while (newHour !== xx.end_hour) {
        min = min + 15;
        if (min === 60) {
          hour = hour + 1;
          min = 0;
        }
        if (hour < 10) {
          newHour = '0' + hour;
        } else {
          newHour = hour;
        }
        if (min < 10) {
          newHour = newHour + ':0' + min;
        } else {
          newHour = newHour + ':' + min;
        }
        newHour = newHour + ':00';
        aux = st.setHours(newHour.split(':')[0], newHour.split(':')[1], 0);
        classP = 'open';
        patient = '';
        for (const yy of this.patients) {
          if ( new Date(yy.medic_attention).toString() === new Date(aux).toString()) {
            classP = 'closed';
            patient = yy.names + ' ' + yy.surnames;
          }
        }
        newArr.push(
          {
            time: aux,
            class: classP,
            patient: patient
          }
        );
      }
      // st.setHours(xx.start_hour.split(':')[0], xx.start_hour.split(':')[1]);
      newArr.pop();
    }
    return newArr;
  }

  changeDoctor() {
    const id = this.options.get('doctor').value;
    this.doctorCalendar = true;
    const doctor = this.personal.find(x => Number(x.id) === Number(id));
    const data = {
      id: doctor.id
    };
    const curr = new Date;
    const first = (curr.getDate() - curr.getDay()) + 1;
    const last = first + 5;

    let aux = new Date(curr.setDate(first)).setHours(0, 0, 0);
    this.firstDay = new Date(aux);

    aux = new Date(curr.setDate(last)).setHours(23, 0, 0);
    this.lastDay = new Date(aux);

    const data2 = {
      from: this.firstDay,
      to: this.lastDay
    };

    this.http.post(localStorage.getItem('baseurl') + '/get_patients', data2)
    .subscribe(
      res => {
        this.patients = JSON.parse(JSON.stringify(res)).data;
        console.log('patients');
        console.log(this.patients);
      },
      error => {
        console.log(error);
      }
    );

    this.http.post(localStorage.getItem('baseurl') + '/get_personal_schedules', data)
    .subscribe(
      res => {
        this.calendar = JSON.parse(JSON.stringify(res)).data;
        //console.log(this.calendar);
      },
      error => {
        console.log(error);
      }
    );
    //console.log(doctor);
  }

  setDateTime(time) {
    if (time.class === 'open') {
      this.medic_attention = time.time;
    }
  }

  getClass(time) {
    if (time.class === 'open') {
      if (time.time !== undefined && this.medic_attention !== undefined) {
        if ( new Date(time.time).toString() === new Date(this.medic_attention).toString() ) {
          return 'selected';
        } else {
          return '';
        }
      }
    }
  }

  save(option: number) {
    let formError = false;

    if (!this.options.valid) {
      formError = true;
      this.toastr.error('Debe ingresar todos los campos', '');
    }

    const aux = new Date(this.medic_attention);

    if ( !formError ) {
      const data = {
        surnames: this.options.get('surname').value,
        names: this.options.get('name').value,
        dni: this.options.get('dni').value,
        type: this.options.get('type').value,
        traumatologist: this.options.get('traum').value === 's' ? true : false,

        physiatrist: this.options.get('fisiat').value === 's' ? true : false,
        operation: this.options.get('oper').value === 's' ? true : false,
        fracture: this.options.get('fractu').value === 's' ? true : false,
        accident: this.options.get('accid').value === 's' ? true : false,
      };

      this.http.post(localStorage.getItem('baseurl') + '/store_patient', data)
      .subscribe(
        res => {
          if (JSON.parse(JSON.stringify(res)).data === 'ok') {
            this.toastr.success('Se guardo con exito', '');
            setTimeout(() => {
              if (option === 1) {
                this.cancel();
              } else {
                this.router.navigate(['addattention']);
              }
            }, 1000);
          } else {
            this.toastr.error('Ocurrio un error al guardar', '');
          }
        },
        error => {
          this.toastr.error('Ocurrio un error al guardar', '');
        }
      );
    }
  }

  cancel() {
    this.options.get('surname').setValue('');
    this.options.get('name').setValue('');
    this.options.get('type').setValue('e');
    this.options.get('traum').setValue('n');
    this.options.get('fisiat').setValue('n');
    this.options.get('doctor').setValue('');
    this.options.get('dni').setValue('');

    this.options.get('oper').setValue('n');
    this.options.get('fractu').setValue('n');
    this.options.get('accid').setValue('n');
  }

}
