import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { Patient } from '../common/patient';
import { Personal } from '../common/personal';
import { Attention } from '../common/attention';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-patient-controller',
  templateUrl: './patient-controller.component.html',
  styleUrls: ['./patient-controller.component.less']
})
export class PatientControllerComponent implements OnInit {

  displayedColumns: string[] = ['personal', 'sessions', 'startdate', 'interval', 'hour', 'hour2', 'delete'];
  dataSource;

  patient: Patient;
  personal: Personal[];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  ngOnInit() {
    this.patient = JSON.parse(localStorage.getItem('patient'));
    const data = {id: this.patient.id};
    this.http.post(localStorage.getItem('baseurl') + '/get_attention_patient', data)
    .subscribe(
      res => {
        console.log(res);
        this.dataSource = new MatTableDataSource<Attention>(JSON.parse(JSON.stringify(res)).data);
        this.dataSource.paginator = this.paginator;
      },
      error => {
        console.log(error);
      }
    );
    this.http.get(localStorage.getItem('baseurl') + '/get_personals')
    .subscribe(
      res => {
        this.personal = JSON.parse(JSON.stringify(res)).data;
        this.personal = this.personal.filter((x) => x.is_doctor === 'n');
      },
      error => {
        console.log(error);
      }
    );
  }

  editPatient(patient) {
    localStorage.setItem('edit', JSON.stringify(patient));
    this.router.navigate(['editpatients']);
  }

  schedulePersonal(patient) {
    localStorage.setItem('attention', JSON.stringify(patient.id));
    this.router.navigate(['/patient_calendar']);
  }

  parsePersonal(id) {
    const p = this.personal.find(x => x.id === id);
    return p.name + ' ' + p.surname;
  }
}
