import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientControllerComponent } from './patient-controller.component';

describe('PatientControllerComponent', () => {
  let component: PatientControllerComponent;
  let fixture: ComponentFixture<PatientControllerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientControllerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
