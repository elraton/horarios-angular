import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addpersonal',
  templateUrl: './addpersonal.component.html',
  styleUrls: ['./addpersonal.component.less']
})
export class AddpersonalComponent implements OnInit {

  options: FormGroup;
  showQuestions = false;

  constructor(
    fb: FormBuilder,
    private http: HttpClient,
    private router: Router,
    private toastr: ToastrService
  ) {
    this.options = fb.group({
      surname: ['', Validators.required],
      name: ['', Validators.required],
      dni: ['', Validators.required],
      is_doctor: ['n', Validators.required],
      is_kids: ['n', Validators.required],
      sede: ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  save() {

    let formError = false;

    if (!this.options.valid) {
      formError = true;
      this.toastr.error('Debe ingresar todos los campos', '');
    }

    if ( !formError ) {
      const data = {
        surname: this.options.get('surname').value,
        name: this.options.get('name').value,
        dni: this.options.get('dni').value,
        is_doctor: this.options.get('is_doctor').value,
        is_kids: this.options.get('is_kids').value,
        sede: this.options.get('sede').value
      };

      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
        })
      };

      this.http.post(localStorage.getItem('baseurl') + '/store_personal', data)
      .subscribe(
        res => {
          if (JSON.parse(JSON.stringify(res)).data === 'ok') {
            this.toastr.success('Se guardo con exito', '');
            setTimeout(() => {
              this.router.navigate(['home']);
            }, 1000);
          } else {
            this.toastr.error('Ocurrio un error al guardar', '');
          }
        },
        error => {
          this.toastr.error('Ocurrio un error al guardar', '');
        }
      );
    }
  }

  cancel() {

  }

}
