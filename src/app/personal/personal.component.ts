import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { Personal } from '../common/personal';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogcalendarComponent } from '../dialogcalendar/dialogcalendar.component';

@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.less']
})
export class PersonalComponent implements OnInit {

  displayedColumns: string[] = ['surname', 'name', 'dni', 'is_doctor', 'delete'];
  dataSource;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private http: HttpClient,
    public dialog: MatDialog,
    private router: Router
  ) { }

  ngOnInit() {
    this.http.get(localStorage.getItem('baseurl') + '/get_personals')
    .subscribe(
      res => {
        this.dataSource = new MatTableDataSource<Personal>(JSON.parse(JSON.stringify(res)).data);
        this.dataSource.paginator = this.paginator;
      },
      error => {
        console.log(error);
      }
    );
  }

  openDialog(action: string, schedule) {
    const dialogRef = this.dialog.open(DialogcalendarComponent, {
      width: '350px',
      data: {action: action}
    });

    dialogRef.afterClosed().subscribe(result => {
      if ( action === 'Eliminar' && result === 'Eliminar') {
        this.deletePersonal(schedule);
      }
    });
  }

  editPersonal(personal) {
    localStorage.setItem('edit', JSON.stringify(personal));
    this.router.navigate(['editpersonal']);
  }

  deletePersonal(personal) {
    // route: destroy_personal
    const data = {id: personal.id};

    this.http.post(localStorage.getItem('baseurl') + '/destroy_personal', data)
    .subscribe(
      res => {
        console.log(res);
        let dataarr = JSON.parse(JSON.stringify(this.dataSource.data));
        dataarr = dataarr.filter(x => x.id !== personal.id);
        this.dataSource = new MatTableDataSource<Personal>(dataarr);
        this.dataSource.paginator = this.paginator;
      },
      error => {
        console.log(error);
      }
    );
  }

  schedulePersonal(personal) {
    localStorage.setItem('schedule', JSON.stringify(personal));
    this.router.navigate(['schedulepersonal']);
  }

}
