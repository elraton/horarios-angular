import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.less']
})
export class FormComponent implements OnInit {
  options: FormGroup;
  showQuestions = false;
  personal = [];

  constructor(
    fb: FormBuilder,
    private http: HttpClient,
    private toastr: ToastrService
  ) {
    this.options = fb.group({
      surname: ['', Validators.required],
      name: ['', Validators.required],
      type: ['e', Validators.required],
      traum: ['n', Validators.required],
      fisiat: ['n', Validators.required],
      doctor: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.http.get(localStorage.getItem('baseurl') + '/get_personals')
    .subscribe(
      res => {
        this.personal = JSON.parse(JSON.stringify(res)).data;
        this.personal = this.personal.filter((x) => x.is_doctor === 's');
      },
      error => {
        console.log(error);
      }
    );
  }

  save() {

    let formError = false;

    if (!this.options.valid) {
      formError = true;
      this.toastr.error('Debe ingresar todos los campos', '');
    }

    if ( !formError ) {
      const data = {
        surname: this.options.get('surname').value,
        name: this.options.get('name').value,
        dni: this.options.get('dni').value
      };

      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
        })
      };

      this.http.post(localStorage.getItem('baseurl') + '/store_personal', data)
      .subscribe(
        res => {
          console.log(res);
        },
        error => {
          console.log(error);
        }
      );
    }
  }

  aParticular(event) {
    console.log(event.value);
    if (event.value === 'p') {
      this.showQuestions = true;
    } else {
      this.showQuestions = false;
    }
  }

  changeDoctor() {
    console.log(this.options.get('doctor').value);
  }

}
