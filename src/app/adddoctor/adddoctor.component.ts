import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastrService, Toast } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Patient } from '../common/patient';
import { DoctorAttention } from '../common/doctorattention';
import { Calendar } from '../common/calendar';
import { Personal } from '../common/personal';

@Component({
  selector: 'app-adddoctor',
  templateUrl: './adddoctor.component.html',
  styleUrls: ['./adddoctor.component.less']
})
export class AdddoctorComponent implements OnInit {

  patient = 0;
  personal = 0;
  doctor = 0;
  hourlv = '';
  hourss = '';

  patients: Patient[];
  personals: Personal[];
  doctors: Personal[];
  calendarDays: Calendar[];

  filteredOptions: Observable<Patient[]>;
  patientControl = new FormControl();

  filteredOptions2: Observable<Personal[]>;
  personalControl = new FormControl();

  filteredOptions3: Observable<Personal[]>;
  doctorControl = new FormControl();

  alldoctorAttentions: DoctorAttention[] = [];
  doctorAttentions: DoctorAttention[];

  doctorCalendar = false;

  firstDay: Date;
  lastDay: Date;
  calendar = [];
  week_next = true;

  medic_attention: Date;

  user;
  showMessage = false;
  message = '';
  messagetype = 1;
  globalAttention;

  calendar_global = [];
  weekdays = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];

  hour_lv = [
    {hour: '07:00 am', value: '07:00'},
    {hour: '08:00 am', value: '08:00'},
    {hour: '09:00 am', value: '09:00'},
    {hour: '10:00 am', value: '10:00'},
    {hour: '10:30 am', value: '10:30'},
    {hour: '11:00 am', value: '11:00'},
    {hour: '11:30 am', value: '11:30'},
    {hour: '12:00 am', value: '12:00'},
    {hour: '12:30 am', value: '12:30'},
    {hour: '01:30 pm', value: '13:30'},
    {hour: '02:00 pm', value: '14:00'},
    {hour: '02:30 pm', value: '14:30'},
    {hour: '03:00 pm', value: '15:00'},
    {hour: '03:30 pm', value: '15:30'},
    {hour: '04:00 pm', value: '16:00'},
    {hour: '04:30 pm', value: '16:30'},
    {hour: '05:00 pm', value: '17:00'},
    {hour: '05:30 pm', value: '17:30'},
    {hour: '06:00 pm', value: '18:00'},
    {hour: '06:30 pm', value: '18:30'},
    {hour: '07:00 pm', value: '19:00'},
  ];
  hour_s = [
    {hour: '07:00 am', value: '07:00'},
    {hour: '07:30 am', value: '07:30'},
    {hour: '08:00 am', value: '08:00'},
    {hour: '08:30 am', value: '08:30'},
    {hour: '09:00 am', value: '09:00'},
    {hour: '09:30 am', value: '09:30'},
    {hour: '10:00 am', value: '10:00'},
    {hour: '10:30 am', value: '10:30'},
    {hour: '11:00 am', value: '11:00'},
    {hour: '11:30 am', value: '11:30'},
    {hour: '12:00 am', value: '12:00'},
    {hour: '12:30 am', value: '12:30'},
    {hour: '01:00 pm', value: '13:00'},
    {hour: '01:30 pm', value: '13:30'},
    {hour: '02:00 pm', value: '14:00'},
    {hour: '02:30 pm', value: '14:30'},
  ];

  _globalAttention;
  constructor(
    fb: FormBuilder,
    private http: HttpClient,
    private router: Router,
    private toastr: ToastrService,
    private location: Location
  ) { }

  ngOnInit() {
    this._globalAttention = JSON.parse(localStorage.getItem('attention'));
    console.log(this._globalAttention);
    this.user = JSON.parse(localStorage.getItem('user'));
    this.http.get(localStorage.getItem('baseurl') + '/get_doctor_attentions')
    .subscribe(
      res => {
        this.alldoctorAttentions = JSON.parse(JSON.stringify(res)).data;
      },
      error => {
        console.log(error);
      }
    );
    this.http.get(localStorage.getItem('baseurl') + '/all_patients')
    .subscribe(
      res => {
        this.patients = JSON.parse(JSON.stringify(res)).data;

        const patt = this.patients.find(x => x.id === this._globalAttention.patient);
        // console.log(this.patients);
        // this.patients = this.patients.filter((x) => x.is_doctor === 'n');
        this.filteredOptions = this.patientControl.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filter(value))
        );

        this.patientControl.setValue(patt.names + ' ' + patt.surnames);
        this.patient = +patt.id;
        this.patientControl.disable();
      },
      error => {
        console.log(error);
      }
    );

    this.http.get(localStorage.getItem('baseurl') + '/get_personals')
    .subscribe(
      res => {
        this.personals = JSON.parse(JSON.stringify(res)).data;
        this.personals = this.personals.filter((x) => x.is_doctor === 'n');
        this.personals = this.personals.filter((x) => x.sede === this.user.sede);

        this.doctors = JSON.parse(JSON.stringify(res)).data;
        this.doctors = this.doctors.filter((x) => x.is_doctor === 's');
        this.filteredOptions2 = this.personalControl.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filter2(value))
        );

        this.filteredOptions3 = this.doctorControl.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filter3(value))
        );
      },
      error => {
        console.log(error);
      }
    );
  }

  doctorChange(id) {
    this.calendar_global = [];
    this.doctor = id;

    this.doctorCalendar = true;
    const doctor = this.doctors.find(x => Number(x.id) === Number(id));
    const data = {
      id: doctor.id
    };
    const curr = new Date;
    const first = (curr.getDate() - curr.getDay()) + 1;
    const last = first + 5;

    let aux = new Date(curr.setDate(first)).setHours(0, 0, 0);
    this.firstDay = new Date(aux);

    aux = new Date(curr.setDate(last)).setHours(23, 0, 0);
    this.lastDay = new Date(aux);

    const data2 = {
      from: this.firstDay,
      to: this.lastDay
    };

    this.http.post(localStorage.getItem('baseurl') + '/get_doctor_attention_range', data2)
    .subscribe(
      res => {
        this.doctorAttentions = JSON.parse(JSON.stringify(res)).data;
        // console.log(this.calendarDays);

        this.http.post(localStorage.getItem('baseurl') + '/get_personal_schedules', data)
        .subscribe(
          res2 => {
            this.calendar = JSON.parse(JSON.stringify(res2)).data;
            this.calendar = this.calendar.filter(x => x.sede === this.user.sede);

            this.calendar_global.push(this.parseDay('Lu', 0));
            this.calendar_global.push(this.parseDay('Ma', 1));
            this.calendar_global.push(this.parseDay('Mi', 2));
            this.calendar_global.push(this.parseDay('Ju', 3));
            this.calendar_global.push(this.parseDay('Vi', 4));
            this.calendar_global.push(this.parseDay('Sa', 5));
          },
          error => {
            console.log(error);
          }
        );
      },
      error => {
        console.log(error);
      }
    );
  }
  cancel() {
    this.location.back();
  }

  patientChange(a) {  }

  save() {
    const dated = new Date(this.medic_attention);
    const schedule = dated.toISOString().split('T')[0] + ' ' + dated.toTimeString().split(' ')[0];
    const patdata = {
      patient: this.patient,
      status: 'active'
    };
    const data = {
      patient: this.patient,
      personal: this.doctor,
      sede: this.user.sede,
      schedule: schedule,
      global: this._globalAttention.id
    };
    const data2 = {
      patient: this.patient,
      attention: this._globalAttention.id
    };
    this.http.post(localStorage.getItem('baseurl') + '/store_doctor_attention', data)
    .subscribe(
      res3 => {
        this.http.post(localStorage.getItem('baseurl') + '/search_pending_boards', data2)
        .subscribe(
          res4 => {
            this.toastr.success('Se guardo correctamente');
            this.location.back();
          },
          error2 => {
            this.toastr.error('Sucedio un error');
          }
        );
      },
      error => {
        this.toastr.error('Sucedio un error');
      }
    );

    /**/
  }

  previus_week() {
    this.week_next = true;
    const curr = new Date;
    const first = (curr.getDate() - curr.getDay()) + 1;
    const last = first + 6;

    let aux = new Date(curr.setDate(first)).setHours(0, 0, 0);
    this.firstDay = new Date(aux);

    aux = new Date(curr.setDate(last)).setHours(0, 0, 0);
    this.lastDay = new Date(aux);

    const data = {
      from: this.firstDay,
      to: this.lastDay
    };

    this.http.post(localStorage.getItem('baseurl') + '/get_calendar_days', data)
    .subscribe(
      res => {
        this.calendarDays = JSON.parse(JSON.stringify(res)).data;
        this.calendar_global = [];
        this.calendar_global.push(this.parseDay('Lu', 0));
        this.calendar_global.push(this.parseDay('Ma', 1));
        this.calendar_global.push(this.parseDay('Mi', 2));
        this.calendar_global.push(this.parseDay('Ju', 3));
        this.calendar_global.push(this.parseDay('Vi', 4));
        this.calendar_global.push(this.parseDay('Sa', 5));
      },
      error => {
        console.log(error);
      }
    );
  }

  next_week() {
    this.week_next = false;
    const curr = new Date;
    const first = (curr.getDate() - curr.getDay()) + 8;
    const last = first + 6;

    let aux = new Date(curr.setDate(first)).setHours(0, 0, 0);
    this.firstDay = new Date(aux);

    aux = new Date(curr.setDate(last)).setHours(0, 0, 0);
    this.lastDay = new Date(aux);

    const data = {
      from: this.firstDay,
      to: this.lastDay
    };

    this.http.post(localStorage.getItem('baseurl') + '/get_calendar_days', data)
    .subscribe(
      res => {
        this.calendarDays = JSON.parse(JSON.stringify(res)).data;

        this.calendar_global = [];
        this.calendar_global.push(this.parseDay('Lu', 0));
        this.calendar_global.push(this.parseDay('Ma', 1));
        this.calendar_global.push(this.parseDay('Mi', 2));
        this.calendar_global.push(this.parseDay('Ju', 3));
        this.calendar_global.push(this.parseDay('Vi', 4));
        this.calendar_global.push(this.parseDay('Sa', 5));
      },
      error => {
        console.log(error);
      }
    );

    const curr2 = new Date;
    const st = new Date(curr2.setDate(this.firstDay.getDate() + 4));
    const aux2 = st.setHours(13, 30, 0);
  }

  setDateTime(time) {
    for (const cal of this.calendar_global) {
      for (const schedule of cal ) {
        if (schedule.class !== 'closed') {
          schedule.class = 'open';
        }
      }
    }
    if (time.class !== 'closed') {
      time.class = time.class + ' selected';
      this.medic_attention = time.time;
    }
    console.log(this.medic_attention);
    /*if (time.class === 'open' && this.options.get('interval').value === '1d') {
      this.medic_attention = time.time;
    }*/
  }

  getClass(time) {
    if (time.class === 'open') {
      if (time.time !== undefined && this.medic_attention !== undefined) {
        if ( new Date(time.time).toString() === new Date(this.medic_attention).toString() ) {
          return 'selected';
        } else {
          return '';
        }
      }
    }
  }

  parseDay(day, weekday) {
    let arr = JSON.parse(JSON.stringify(this.calendar));
    arr = arr.filter((vv) => vv.day === day);
    if (arr.length <= 0) {
      return [];
    }
    let newArr = [];
    const curr = new Date;
    curr.setHours(9, 0, 0);
    let st = new Date(curr.setDate(this.firstDay.getDate() + weekday));
    const p_personal = this.doctors.find(x => x.id === arr[0].personal);
    for ( const xx of arr ) {
      let newHour = xx.start_hour;
      let hour = Number(newHour.split(':')[0]);
      const min_hour = Number(newHour.split(':')[1]);
      let min = Number(newHour.split(':')[1]);
      let aux = st.setHours(newHour.split(':')[0], newHour.split(':')[1], 0);
      let classP = 'open';
      let patient = '';
      let p_type = '';
      let count = 0;
      while (newHour !== xx.end_hour) {
        aux = st.setHours(newHour.split(':')[0], newHour.split(':')[1], 0);
        classP = 'open';
        patient = '';
        p_type = '';

        for (const yy of this.doctorAttentions) {
          if ( new Date(yy.schedule).toString() === new Date(aux).toString()) {
            if (Number(yy.personal) === Number(this.doctor)) {
              classP = 'closed';
              const patt = this.patients.find(val => val.id === yy.patient);
              p_type = patt.type;
              if (patt.type !== 'Particular') {
                patient = patient + patt.names.toLowerCase() + ' ' + patt.surnames.toLowerCase() + '(S)' + '<br>';
              } else {
                patient = patient + patt.names.toUpperCase() + ' ' + patt.surnames.toUpperCase() + '(P)' + '<br>';
              }
            }
          }
        }

        newArr.push(
          {
            time: aux,
            class: classP,
            patient: patient,
            type: p_type
          }
        );
        if (p_personal.is_kids === 's') {
          min = min + 30;
          if (min === 60) {
            hour = hour + 1;
            min = 0;
          }
        } else {
          min = min + 15;
          if (min === 60) {
            hour = hour + 1;
            min = 0;
          }
        }

        if (hour < 10) {
          newHour = '0' + hour;
        } else {
          newHour = hour;
        }
        if (min < 10) {
          newHour = newHour + ':0' + min;
        } else {
          newHour = newHour + ':' + min;
        }
        newHour = newHour + ':00';
        count = count + 1;
        if (count >= 100 ) {
          break;
        }
      }
      // st.setHours(xx.start_hour.split(':')[0], xx.start_hour.split(':')[1]);
    }
    return newArr;
  }

  private _filter3(value: string): Personal[] {
    const filterValue = value.toLowerCase();

    return this.doctors.filter((option) => {
      if ( option.surname.toLowerCase().includes(filterValue) ) {
        return option;
      }
      if ( option.name.toLowerCase().includes(filterValue) ) {
        return option;
      }
      if ( option.dni.toLowerCase().includes(filterValue) ) {
        return option;
      }
    });
  }

  private _filter2(value: string): Personal[] {
    const filterValue = value.toLowerCase();

    return this.personals.filter((option) => {
      if ( option.surname.toLowerCase().includes(filterValue) ) {
        return option;
      }
      if ( option.name.toLowerCase().includes(filterValue) ) {
        return option;
      }
      if ( option.dni.toLowerCase().includes(filterValue) ) {
        return option;
      }
    });
  }

  private _filter(value: string): Patient[] {
    const filterValue = value.toLowerCase();

    return this.patients.filter((option) => {
      if ( option.surnames.toLowerCase().includes(filterValue) ) {
        return option;
      }
      if ( option.names.toLowerCase().includes(filterValue) ) {
        return option;
      }
      if ( option.dni.toLowerCase().includes(filterValue) ) {
        return option;
      }
    });
  }

}
