import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { Patient } from '../common/patient';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.less']
})
export class PatientsComponent implements OnInit {

  displayedColumns: string[] = ['surname', 'name', 'dni', 'type', 'delete'];
  dataSource;
  data = [];

  search = '';
  isSearching = false;
  searchdata = [];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  ngOnInit() {
    this.http.get(localStorage.getItem('baseurl') + '/all_patients')
    .subscribe(
      res => {
        this.data = JSON.parse(JSON.stringify(res)).data;
        this.dataSource = new MatTableDataSource<Patient>(this.data);
        this.dataSource.paginator = this.paginator;
      },
      error => {
        console.log(error);
      }
    );
  }

  editPatient(patient) {
    localStorage.setItem('edit', JSON.stringify(patient));
    this.router.navigate(['editpatients']);
  }

  schedulePersonal(patient) {
    localStorage.setItem('patient', JSON.stringify(patient));
    this.router.navigate(['/patient_attentions']);
  }

  closesearch() {
    this.isSearching = false;
    this.search = '';
    this.dataSource = new MatTableDataSource<Patient>(this.data);
    this.dataSource.paginator = this.paginator;
  }

  filterPatient() {
    if (this.search.length > 3) {
      this.isSearching = true;
      this.searchdata = JSON.parse(JSON.stringify(this.data));

      this.searchdata = this.searchdata.filter((x) => {
        if (x.names.toLowerCase().indexOf(this.search.toLowerCase()) !== -1) {
          return x;
        }
        if (x.surnames.toLowerCase().indexOf(this.search.toLowerCase()) !== -1) {
          return x;
        }
        if (x.dni.toLowerCase().indexOf(this.search.toLowerCase()) !== -1) {
          return x;
        }
      });
      this.dataSource = new MatTableDataSource<Patient>(this.searchdata);
      this.dataSource.paginator = this.paginator;
    }
  }
}
