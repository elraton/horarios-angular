import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Pending } from '../common/pending';
import { Patient } from '../common/patient';
import { Attention } from '../common/attention';

@Component({
  selector: 'app-pendingboard',
  templateUrl: './pendingboard.component.html',
  styleUrls: ['./pendingboard.component.less']
})
export class PendingboardComponent implements OnInit {

  pendings: Pending[] = [];
  patients: Patient[] = [];
  attentions = [];
  user;

  displayedColumns: string[] = ['attention', 'patient', 'message'];
  dataSource;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private router: Router,
    private http: HttpClient,
  ) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.http.get(localStorage.getItem('baseurl') + '/get_pending_boards')
    .subscribe(
      res => {
        this.pendings = JSON.parse(JSON.stringify(res)).data;
        this.pendings = this.pendings.filter( x => x.user === this.user.id);
        this.dataSource = new MatTableDataSource<Pending>(this.pendings);
        this.dataSource.paginator = this.paginator;
      },
      error => {
        console.log(error);
      }
    );

    this.http.get(localStorage.getItem('baseurl') + '/all_patients')
    .subscribe(
      res => {
        this.patients = JSON.parse(JSON.stringify(res)).data;
      },
      error => {
        console.log(error);
      }
    );

    this.http.get(localStorage.getItem('baseurl') + '/get_global_attentions')
    .subscribe(
      res => {
        this.attentions = JSON.parse(JSON.stringify(res)).data;
      },
      error => {
        console.log(error);
      }
    );
  }

  parsePatient(id) {
    if (this.patients.length > 0) {
      const patient = this.patients.find( x => x.id === id);
      return patient.names + ' ' + patient.surnames;
    } else {
      return '';
    }
  }

  editPatient(element) {
    const data = {
      id: element.attention,
      patient: element.patient
    };
    localStorage.setItem('attention', JSON.stringify(data));
    this.router.navigate(['/adddoctor']);
  }

}
