import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingboardComponent } from './pendingboard.component';

describe('PendingboardComponent', () => {
  let component: PendingboardComponent;
  let fixture: ComponentFixture<PendingboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
