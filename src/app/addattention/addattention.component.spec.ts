import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddattentionComponent } from './addattention.component';

describe('AddattentionComponent', () => {
  let component: AddattentionComponent;
  let fixture: ComponentFixture<AddattentionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddattentionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddattentionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
