import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastrService, Toast } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Patient } from '../common/patient';
import { DoctorAttention } from '../common/doctorattention';
import { Calendar } from '../common/calendar';
import { Personal } from '../common/personal';

@Component({
  selector: 'app-addattention',
  templateUrl: './addattention.component.html',
  styleUrls: ['./addattention.component.less']
})
export class AddattentionComponent implements OnInit {

  options: FormGroup;
  showQuestions = false;

  step = 1;
  laststep = 0;

  patient = 0;
  personal = 0;
  doctor = 0;
  hourlv = '';
  hourss = '';

  patients: Patient[];
  personals: Personal[];
  doctors: Personal[];
  calendarDays: Calendar[];

  filteredOptions: Observable<Patient[]>;
  patientControl = new FormControl();

  filteredOptions2: Observable<Personal[]>;
  personalControl = new FormControl();

  filteredOptions3: Observable<Personal[]>;
  doctorControl = new FormControl();

  filteredOptions4: Observable<any[]>;
  hourcontrol1 = new FormControl();

  filteredOptions5: Observable<any[]>;
  hourcontrol2 = new FormControl();

  alldoctorAttentions: DoctorAttention[] = [];
  doctorAttentions: DoctorAttention[];

  doctorCalendar = false;

  firstDay: Date;
  lastDay: Date;
  calendar = [];
  week_next = true;

  medic_attention: Date;

  user;
  showMessage = false;
  message = '';
  messagetype = 1;
  globalAttention;

  calendar_global = [];
  weekdays = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];

  hour_lv = [
    {hour: '07:00 am', value: '07:00'},
    {hour: '08:00 am', value: '08:00'},
    {hour: '09:00 am', value: '09:00'},
    {hour: '10:00 am', value: '10:00'},
    {hour: '10:30 am', value: '10:30'},
    {hour: '11:00 am', value: '11:00'},
    {hour: '11:30 am', value: '11:30'},
    {hour: '12:00 am', value: '12:00'},
    {hour: '12:30 am', value: '12:30'},
    {hour: '01:30 pm', value: '13:30'},
    {hour: '02:00 pm', value: '14:00'},
    {hour: '02:30 pm', value: '14:30'},
    {hour: '03:00 pm', value: '15:00'},
    {hour: '03:30 pm', value: '15:30'},
    {hour: '04:00 pm', value: '16:00'},
    {hour: '04:30 pm', value: '16:30'},
    {hour: '05:00 pm', value: '17:00'},
    {hour: '05:30 pm', value: '17:30'},
    {hour: '06:00 pm', value: '18:00'},
    {hour: '06:30 pm', value: '18:30'},
    {hour: '07:00 pm', value: '19:00'},
  ];
  hour_s = [
    {hour: '07:00 am', value: '07:00'},
    {hour: '07:30 am', value: '07:30'},
    {hour: '08:00 am', value: '08:00'},
    {hour: '08:30 am', value: '08:30'},
    {hour: '09:00 am', value: '09:00'},
    {hour: '09:30 am', value: '09:30'},
    {hour: '10:00 am', value: '10:00'},
    {hour: '10:30 am', value: '10:30'},
    {hour: '11:00 am', value: '11:00'},
    {hour: '11:30 am', value: '11:30'},
    {hour: '12:00 am', value: '12:00'},
    {hour: '12:30 am', value: '12:30'},
    {hour: '01:00 pm', value: '13:00'},
    {hour: '01:30 pm', value: '13:30'},
    {hour: '02:00 pm', value: '14:00'},
    {hour: '02:30 pm', value: '14:30'},
  ];

  constructor(
    fb: FormBuilder,
    private http: HttpClient,
    private router: Router,
    private toastr: ToastrService
  ) {
    this.options = fb.group({
      sessions: ['0', Validators.required],
      date_init: ['', Validators.required],
      interval: ['Lu-Ma-Mi-Ju-Vi', Validators.required],
      /*hour: ['', Validators.required],
      hour2: [''],*/
    });
  }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.http.get(localStorage.getItem('baseurl') + '/get_doctor_attentions')
    .subscribe(
      res => {
        this.alldoctorAttentions = JSON.parse(JSON.stringify(res)).data;
      },
      error => {
        console.log(error);
      }
    );
    this.filteredOptions4 = this.hourcontrol1.valueChanges
    .pipe(
      startWith(''),
      map(value => this._filter4(value))
    );
    this.filteredOptions5 = this.hourcontrol2.valueChanges
    .pipe(
      startWith(''),
      map(value => this._filter5(value))
    );
    this.http.get(localStorage.getItem('baseurl') + '/all_patients')
    .subscribe(
      res => {
        this.patients = JSON.parse(JSON.stringify(res)).data;
        // console.log(this.patients);
        // this.patients = this.patients.filter((x) => x.is_doctor === 'n');
        this.filteredOptions = this.patientControl.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filter(value))
        );
      },
      error => {
        console.log(error);
      }
    );

    this.http.get(localStorage.getItem('baseurl') + '/get_personals')
    .subscribe(
      res => {
        this.personals = JSON.parse(JSON.stringify(res)).data;
        this.personals = this.personals.filter((x) => x.is_doctor === 'n');
        this.personals = this.personals.filter((x) => x.sede === this.user.sede);

        this.doctors = JSON.parse(JSON.stringify(res)).data;
        this.doctors = this.doctors.filter((x) => x.is_doctor === 's');
        this.filteredOptions2 = this.personalControl.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filter2(value))
        );

        this.filteredOptions3 = this.doctorControl.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filter3(value))
        );
      },
      error => {
        console.log(error);
      }
    );

  }

  focusDate() {
    console.log('alertita');
  }

  patientChange(id) {
    this.patient = id;
  }

  doctorChange(id) {
    this.calendar_global = [];
    this.doctor = id;

    this.doctorCalendar = true;
    const doctor = this.doctors.find(x => Number(x.id) === Number(id));
    const data = {
      id: doctor.id
    };
    const curr = new Date;
    const first = (curr.getDate() - curr.getDay()) + 1;
    const last = first + 5;

    let aux = new Date(curr.setDate(first)).setHours(0, 0, 0);
    this.firstDay = new Date(aux);

    aux = new Date(curr.setDate(last)).setHours(23, 0, 0);
    this.lastDay = new Date(aux);

    const data2 = {
      from: this.firstDay,
      to: this.lastDay
    };

    this.http.post(localStorage.getItem('baseurl') + '/get_doctor_attention_range', data2)
    .subscribe(
      res => {
        this.doctorAttentions = JSON.parse(JSON.stringify(res)).data;
        // console.log(this.calendarDays);

        this.http.post(localStorage.getItem('baseurl') + '/get_personal_schedules', data)
        .subscribe(
          res2 => {
            this.calendar = JSON.parse(JSON.stringify(res2)).data;
            this.calendar = this.calendar.filter(x => x.sede === this.user.sede);

            this.calendar_global.push(this.parseDay('Lu', 0));
            this.calendar_global.push(this.parseDay('Ma', 1));
            this.calendar_global.push(this.parseDay('Mi', 2));
            this.calendar_global.push(this.parseDay('Ju', 3));
            this.calendar_global.push(this.parseDay('Vi', 4));
            this.calendar_global.push(this.parseDay('Sa', 5));
          },
          error => {
            console.log(error);
          }
        );
      },
      error => {
        console.log(error);
      }
    );
  }

  personalChange(id) {

    this.calendar_global = [];
    this.personal = id;

    this.doctorCalendar = true;
    const doctor = this.personals.find(x => Number(x.id) === Number(id));
    const data = {
      id: doctor.id
    };
    const curr = new Date;
    const first = (curr.getDate() - curr.getDay()) + 1;
    const last = first + 5;

    let aux = new Date(curr.setDate(first)).setHours(0, 0, 0);
    this.firstDay = new Date(aux);

    aux = new Date(curr.setDate(last)).setHours(23, 0, 0);
    this.lastDay = new Date(aux);

    const data2 = {
      from: this.firstDay,
      to: this.lastDay
    };

    this.http.post(localStorage.getItem('baseurl') + '/get_calendar_days', data2)
    .subscribe(
      res => {
        this.calendarDays = JSON.parse(JSON.stringify(res)).data;
        // console.log(this.calendarDays);

        this.http.post(localStorage.getItem('baseurl') + '/get_personal_schedules', data)
        .subscribe(
          res2 => {
            this.calendar = JSON.parse(JSON.stringify(res2)).data;

            this.calendar_global.push(this.parseDay('Lu', 0));
            this.calendar_global.push(this.parseDay('Ma', 1));
            this.calendar_global.push(this.parseDay('Mi', 2));
            this.calendar_global.push(this.parseDay('Ju', 3));
            this.calendar_global.push(this.parseDay('Vi', 4));
            this.calendar_global.push(this.parseDay('Sa', 5));

            console.log(this.calendar_global);
          },
          error => {
            console.log(error);
          }
        );
      },
      error => {
        console.log(error);
      }
    );
    // console.log(doctor);
  }

  previus_week() {
    this.week_next = true;
    const curr = new Date;
    const first = (curr.getDate() - curr.getDay()) + 1;
    const last = first + 6;

    let aux = new Date(curr.setDate(first)).setHours(0, 0, 0);
    this.firstDay = new Date(aux);

    aux = new Date(curr.setDate(last)).setHours(0, 0, 0);
    this.lastDay = new Date(aux);

    const data = {
      from: this.firstDay,
      to: this.lastDay
    };

    this.http.post(localStorage.getItem('baseurl') + '/get_calendar_days', data)
    .subscribe(
      res => {
        this.calendarDays = JSON.parse(JSON.stringify(res)).data;
        this.calendar_global = [];
        this.calendar_global.push(this.parseDay('Lu', 0));
        this.calendar_global.push(this.parseDay('Ma', 1));
        this.calendar_global.push(this.parseDay('Mi', 2));
        this.calendar_global.push(this.parseDay('Ju', 3));
        this.calendar_global.push(this.parseDay('Vi', 4));
        this.calendar_global.push(this.parseDay('Sa', 5));
      },
      error => {
        console.log(error);
      }
    );
  }

  next_week() {
    this.week_next = false;
    const curr = new Date;
    const first = (curr.getDate() - curr.getDay()) + 8;
    const last = first + 6;

    let aux = new Date(curr.setDate(first)).setHours(0, 0, 0);
    this.firstDay = new Date(aux);

    aux = new Date(curr.setDate(last)).setHours(0, 0, 0);
    this.lastDay = new Date(aux);

    const data = {
      from: this.firstDay,
      to: this.lastDay
    };

    this.http.post(localStorage.getItem('baseurl') + '/get_calendar_days', data)
    .subscribe(
      res => {
        this.calendarDays = JSON.parse(JSON.stringify(res)).data;

        this.calendar_global = [];
        this.calendar_global.push(this.parseDay('Lu', 0));
        this.calendar_global.push(this.parseDay('Ma', 1));
        this.calendar_global.push(this.parseDay('Mi', 2));
        this.calendar_global.push(this.parseDay('Ju', 3));
        this.calendar_global.push(this.parseDay('Vi', 4));
        this.calendar_global.push(this.parseDay('Sa', 5));
      },
      error => {
        console.log(error);
      }
    );

    const curr2 = new Date;
    const st = new Date(curr2.setDate(this.firstDay.getDate() + 4));
    const aux2 = st.setHours(13, 30, 0);
  }

  parseDay(day, weekday) {
    let arr = JSON.parse(JSON.stringify(this.calendar));
    arr = arr.filter((vv) => vv.day === day);
    if (arr.length <= 0) {
      return [];
    }
    let newArr = [];
    const curr = new Date;
    curr.setHours(9, 0, 0);
    let st = new Date(curr.setDate(this.firstDay.getDate() + weekday));
    let p_personal;
    if (this.step === 2) {
      p_personal = this.doctors.find(x => x.id === arr[0].personal);
    } else {
      p_personal = this.personals.find(x => x.id === arr[0].personal);
    }
    for ( const xx of arr ) {
      let newHour = xx.start_hour;
      let hour = Number(newHour.split(':')[0]);
      const min_hour = Number(newHour.split(':')[1]);
      let min = Number(newHour.split(':')[1]);
      let aux = st.setHours(newHour.split(':')[0], newHour.split(':')[1], 0);
      let classP = 'open';
      let patient = '';
      let p_type = '';
      let count = 0;
      while (newHour !== xx.end_hour) {
        aux = st.setHours(newHour.split(':')[0], newHour.split(':')[1], 0);
        classP = 'open';
        patient = '';
        p_type = '';
        if (this.step === 2) {
          for (const yy of this.doctorAttentions) {
            if ( new Date(yy.schedule).toString() === new Date(aux).toString()) {
              if (Number(yy.personal) === Number(this.doctor)) {
                classP = 'closed';
                const patt = this.patients.find(val => val.id === yy.patient);
                p_type = patt.type;
                if (patt.type !== 'Particular') {
                  patient = patient + patt.names.toLowerCase() + ' ' + patt.surnames.toLowerCase() + '(S)' + '<br>';
                } else {
                  patient = patient + patt.names.toUpperCase() + ' ' + patt.surnames.toUpperCase() + '(P)' + '<br>';
                }
              }
            }
          }
        } else {
          for (const yy of this.calendarDays) {
            if ( new Date(yy.schedule).toString() === new Date(aux).toString()) {
              if (Number(yy.personal) === Number(this.personal)) {
                classP = 'closed';
                const patt = this.patients.find(val => val.id === yy.patient);
                p_type = patt.type;
                if (patt.type !== 'Particular') {
                  patient = patient + patt.names.toLowerCase() + ' ' + patt.surnames.toLowerCase() + '(S)' + '<br>';
                } else {
                  patient = patient + patt.names.toUpperCase() + ' ' + patt.surnames.toUpperCase() + '(P)' + '<br>';
                }
              }
            }
          }
        }
        newArr.push(
          {
            time: aux,
            class: classP,
            patient: patient,
            type: p_type
          }
        );
        if (p_personal.is_kids === 's') {
          min = min + 30;
          if (min === 60) {
            hour = hour + 1;
            min = 0;
          }
        } else {
          if (this.step === 2) {
            min = min + 15;
            if (min === 60) {
              hour = hour + 1;
              min = 0;
            }
          } else {
            hour = hour + 1;
          }
        }

        if (hour < 10) {
          newHour = '0' + hour;
        } else {
          newHour = hour;
        }
        if (min < 10) {
          newHour = newHour + ':0' + min;
        } else {
          newHour = newHour + ':' + min;
        }
        newHour = newHour + ':00';
        count = count + 1;
        if (count >= 100 ) {
          break;
        }
      }
      // st.setHours(xx.start_hour.split(':')[0], xx.start_hour.split(':')[1]);
    }
    return newArr;
  }

  setDateTime(time) {
    for (const cal of this.calendar_global) {
      for (const schedule of cal ) {
        if (schedule.class !== 'closed') {
          schedule.class = 'open';
        }
      }
    }
    if (time.class !== 'closed') {
      time.class = time.class + ' selected';
      this.medic_attention = time.time;
    }
    console.log(this.medic_attention);
    /*if (time.class === 'open' && this.options.get('interval').value === '1d') {
      this.medic_attention = time.time;
    }*/
  }

  getClass(time) {
    if (time.class === 'open') {
      if (time.time !== undefined && this.medic_attention !== undefined) {
        if ( new Date(time.time).toString() === new Date(this.medic_attention).toString() ) {
          return 'selected';
        } else {
          return '';
        }
      }
    }
  }

  save() {
    let error = false;
    if (!this.options.valid) {
      error = true;
      this.toastr.error('Debe ingresar todos los campos');
    }
    if (this.personal === 0) {
      error = true;
      this.toastr.error('Debe elegir un Fisioterapeuta');
    }
    if (this.patient === 0) {
      error = true;
      this.toastr.error('Debe elegir un Paciente');
    }

    if (this.hourlv === '') {
      error = true;
      this.toastr.error('Debe elegir un horario de Lunes a Viernes');
    }

    if (this.options.get('interval').value === 'Lu-Ma-Mi-Ju-Vi-Sa' || this.options.get('interval').value === 'Ma-Ju-Sa') {
      if (this.hourss === '') {
        error = true;
        this.toastr.error('debes elegir un horario para el Sabado');
      }
    }

    if (error === false) {
      const dates = [];

      let gll = 0;
      if (typeof(this.globalAttention) === 'number') {
        gll = this.globalAttention;
      } else {
        gll = this.globalAttention.id;
      }

      const data = {
        patient: this.patient,
        sessions: this.options.get('sessions').value,
        startdate: this.options.get('date_init').value.toISOString().split('T')[0] + ' ' + this.options.get('date_init').value.toTimeString().split(' ')[0],
        interval: this.options.get('interval').value,
        hour: this.hourlv,
        hour2: this.hourss,
        personal: this.personal,
        schedule: dates,
        global: gll
      };

      const startdate = new Date(data.startdate);
      const curr = startdate;
      let st;
      const aux = this.hourlv.split(':');
      const aux2 = this.hourss.split(':');

      if (this.options.get('date_init').value.getDay() === 6) {
        this.options.get('date_init').value.setHours(aux2[0], aux2[1], 0);
      } else {
        this.options.get('date_init').value.setHours(aux[0], aux[1], 0);
      }
      dates.push(
        this.options.get('date_init').value.toISOString()
        .split('T')[0] + ' ' + this.options.get('date_init').value.toTimeString().split(' ')[0]);
      let interval = 1;
      if (this.options.get('interval').value === 'Lu-Mi-Vi' || this.options.get('interval').value === 'Ma-Ju-Sa') {
        interval = 2;
      }
      for (let i = 1; i < data.sessions; i++) {
        st = new Date(curr.setDate(startdate.getDate() + interval));
        if (this.options.get('interval').value === 'Lu-Ma-Mi-Ju-Vi') {
          if (st.getDay() === 0) {
            st = new Date(curr.setDate(startdate.getDate() + 1 ));
          }
          if (st.getDay() === 6) {
            st = new Date(curr.setDate(startdate.getDate() + 2 ));
          }
        }

        if (this.options.get('interval').value === 'Lu-Ma-Mi-Ju-Vi-Sa') {
          if (st.getDay() === 0) {
            st = new Date(curr.setDate(startdate.getDate() + 1 ));
          }
        }

        if (this.options.get('interval').value === 'Lu-Mi-Vi') {
          if (st.getDay() === 0) {
            st = new Date(curr.setDate(startdate.getDate() + 1 ));
          }
        }

        if (this.options.get('interval').value === 'Ma-Ju-Sa') {
          if (st.getDay() === 1) {
            st = new Date(curr.setDate(startdate.getDate() + 1 ));
          }
        }

        if (st.getDay() === 6) {
          st.setHours(aux2[0], aux2[1], 0);
        } else {
          st.setHours(aux[0], aux[1], 0);
        }

        // añadir setTime para poner las horas correspondientes a los dias, y ya queda listo para guardar :D

        dates.push(st.toISOString().split('T')[0] + ' ' + st.toTimeString().split(' ')[0]);
      }
      // console.log(data);
      this.http.post(localStorage.getItem('baseurl') + '/store_attention', data)
      .subscribe(
        res => {
          console.log(res);
          if (JSON.parse(JSON.stringify(res)).data === 'ok') {
            this.toastr.success('Se guardo con exito', '');
            this.router.navigate(['/schedule']);
          } else {
            this.toastr.error('Ocurrio un error al guardar', '');
          }
        },
        error1 => {
          this.toastr.error('Ocurrio un error al guardar', '');
        }
      );
    }
  }


  saveDoctor() {
    const dated = new Date(this.medic_attention);
    const schedule = dated.toISOString().split('T')[0] + ' ' + dated.toTimeString().split(' ')[0];
    const patdata = {
      patient: this.patient,
      status: 'active'
    };
    this.http.post(localStorage.getItem('baseurl') + '/get_global_attention', patdata)
    .subscribe(
      res => {
        let global = [];
        global = JSON.parse(JSON.stringify(res)).data;
        if (global.length > 0) {
          global = global.filter(x => x.status === 'active');
          const globaldata = {
            global: global[0].id,
          };
          this.http.post(localStorage.getItem('baseurl') + '/get_calendar_by_global', globaldata)
          .subscribe(
            res2 => {
              const calendars: Calendar[] = JSON.parse(JSON.stringify(res2));
              if (new Date(calendars[0].schedule) < new Date()) {
                if (new Date() < new Date(calendars[calendars.length].schedule)) {
                  // show message
                  this.showMessage = true;
                  this.message = 'Ya hay una atención activa, ¿desea agregarla a esta?';
                  this.messagetype = 2;
                  this.globalAttention = global[0];
                }
              }
            },
            error2 => {
              console.log(error2);
            }
          );
        } else {
          this.http.post(localStorage.getItem('baseurl') + '/store_global_attention', patdata)
          .subscribe(
            res2 => {
              const globalid = JSON.parse(JSON.stringify(res2)).data;
              const data = {
                patient: this.patient,
                personal: this.doctor,
                sede: this.user.sede,
                schedule: schedule,
                global: globalid
              };
              this.http.post(localStorage.getItem('baseurl') + '/store_doctor_attention', data)
                .subscribe(
                  res3 => {
                    this.toastr.success('Se guardo correctamente');
                    this.toastr.success('Ya puedes ingresar Atención Fisioterapeutica');
                    this.step = 3; // go to terapist attention
                    this.laststep = 2;
                    this.calendar_global = [];
                  },
                  error => {
                    this.toastr.error('Sucedio un error');
                  }
                );
            },
            error2 => {
              console.log(error2);
            }
          );
        }
      },
      error => {
        console.log(error);
      }
    );

    /**/
  }

  cancel() {
    this.step = this.laststep;
    if (this.step === 1) {
      this.patientControl.enable();
    }
  }

  private _filter5(value: string): any[] {
    const filterValue = value.toLowerCase();
    const hours = JSON.parse(JSON.stringify(this.hour_s));

    return hours.filter((option) => {
      if ( option.hour.toLowerCase().includes(filterValue) ) {
        return option;
      }
    });
  }

  private _filter4(value: string): any[] {
    const filterValue = value.toLowerCase();
    const hours = JSON.parse(JSON.stringify(this.hour_lv));

    return hours.filter((option) => {
      if ( option.hour.toLowerCase().includes(filterValue) ) {
        return option;
      }
    });
  }

  private _filter3(value: string): Personal[] {
    const filterValue = value.toLowerCase();

    return this.doctors.filter((option) => {
      if ( option.surname.toLowerCase().includes(filterValue) ) {
        return option;
      }
      if ( option.name.toLowerCase().includes(filterValue) ) {
        return option;
      }
      if ( option.dni.toLowerCase().includes(filterValue) ) {
        return option;
      }
    });
  }

  private _filter2(value: string): Personal[] {
    const filterValue = value.toLowerCase();

    return this.personals.filter((option) => {
      if ( option.surname.toLowerCase().includes(filterValue) ) {
        return option;
      }
      if ( option.name.toLowerCase().includes(filterValue) ) {
        return option;
      }
      if ( option.dni.toLowerCase().includes(filterValue) ) {
        return option;
      }
    });
  }

  private _filter(value: string): Patient[] {
    const filterValue = value.toLowerCase();

    return this.patients.filter((option) => {
      if ( option.surnames.toLowerCase().includes(filterValue) ) {
        return option;
      }
      if ( option.names.toLowerCase().includes(filterValue) ) {
        return option;
      }
      if ( option.dni.toLowerCase().includes(filterValue) ) {
        return option;
      }
    });
  }

  Si() {
    if (this.messagetype === 1) {
      if (this.step === 2) {

        const patdata = {
          patient: this.patient,
          status: 'active'
        };
        if (this.globalAttention === null) { // no tiene atención activa
          this.http.post(localStorage.getItem('baseurl') + '/store_global_attention', patdata)
          .subscribe(
            res1 => {

              this.globalAttention = JSON.parse(JSON.stringify(res1)).data;
              let gl = 0;
              if (typeof(this.globalAttention) === 'number') {
                gl = this.globalAttention;
              } else {
                gl = this.globalAttention.id;
              }
              const data = {
                patient: this.patient,
                attention: gl,
                message: 'Le falta atención con el médico',
                level: 0,
                user: this.user.id,
              };
              this.http.post(localStorage.getItem('baseurl') + '/store_pending_board', data)
              .subscribe(
                res => {
                  this.step = 3; // go to terapist attention
                  this.laststep = 2;
                  this.calendar_global = [];
                },
                error => {
                  console.log(error);
                }
              );
            },
            error => {
              console.log(error);
            }
          );
        } else { // ya existe una atención activa
          let gll = 0;
          if (typeof(this.globalAttention) === 'number') {
            gll = this.globalAttention;
          } else {
            gll = this.globalAttention.id;
          }
          const data = {
            patient: this.patient,
            attention: gll,
            message: 'Le falta atención con el médico',
            level: 0,
            user: this.user.id,
          };
          this.http.post(localStorage.getItem('baseurl') + '/store_pending_board', data)
          .subscribe(
            res => {
              this.step = 3; // go to terapist attention
              this.laststep = 2;
              this.calendar_global = [];
            },
            error => {
              console.log(error);
            }
          );
        }
      }
    }
    if (this.messagetype === 2) {
      const dated = new Date(this.medic_attention);
      const schedule = dated.toISOString().split('T')[0] + ' ' + dated.toTimeString().split(' ')[0];
      const data = {
        patient: this.patient,
        personal: this.doctor,
        sede: this.user.sede,
        schedule: schedule,
        global: this.globalAttention
      };
      this.http.post(localStorage.getItem('baseurl') + '/store_doctor_attention', data)
        .subscribe(
          res => {
            this.toastr.success('Se guardo correctamente');
            this.toastr.success('Ya puedes ingresar Atención Fisioterapeutica');
            this.step = 3; // go to terapist attention
            this.laststep = 2;
            this.calendar_global = [];
          },
          error => {
            this.toastr.error('Sucedio un error');
          }
        );
    }
  }

  No() {
    this.showMessage = false;
  }

  stepChange(step: number) {
    if (step === 1) { // search to patient
      const patient = this.patients.find(x => x.id === this.patient);

      const data2 = {
        patient: this.patient
      };
      this.http.post(localStorage.getItem('baseurl') + '/get_global_attention', data2)
      .subscribe(
        res => {
          this.globalAttention = null;
          this.globalAttention = JSON.parse(JSON.stringify(res)).data;
          if (this.globalAttention !== null) {
            const att = this.alldoctorAttentions.find(x => x.global === this.globalAttention.id);
            if (att === undefined) {
              if (patient.type === 'Particular') {
                if (patient.physiatrist || patient.traumatologist) {
                  this.step = 3; // go to terapist attention
                  this.laststep = 1;
                  this.patientControl.disable();
                } else {
                  this.step = 2;
                  this.laststep = 1;
                  this.patientControl.disable();
                }
              } else {
                this.step = 2;
                this.laststep = 1;
                this.patientControl.disable();
              }
            } else {
              this.step = 3; // go to terapist attention
              this.laststep = 1;
              this.patientControl.disable();
            }
          } else {
            if (patient.type === 'Particular') {
              if (patient.physiatrist || patient.traumatologist) {
                this.step = 3; // go to terapist attention
                this.laststep = 1;
                this.patientControl.disable();
              } else {
                this.step = 2;
                this.laststep = 1;
                this.patientControl.disable();
              }
            } else {
              this.step = 2;
              this.laststep = 1;
              this.patientControl.disable();
            }
          }
          // console.log(this.calendarDays);
        },
        error => {
          console.log(error);
        }
      );
    }

    if (step === 2) { // set medic attention or add a pending board
      if (this.medic_attention === undefined) {
        this.showMessage = true;
        this.message = '¿Tienes autorizado agregarle atención fisioterapeutica sin atención médica?';
        this.messagetype = 1;
      } else {
        this.saveDoctor();
      }
    }
  }

  hourChange(value, option: number) {
    if (option === 1) {
      this.hourlv = value;
    } else {
      this.hourss = value;
    }
  }
}
